"use strict";
/* jshint ignore:start */

/* jshint ignore:end */

define('dts/adapters/application', ['exports', 'dts/config/environment', 'firebase', 'emberfire/adapters/firebase'], function (exports, _dtsConfigEnvironment, _firebase, _emberfireAdaptersFirebase) {
  exports['default'] = _emberfireAdaptersFirebase['default'].extend({
    firebase: new _firebase['default'](_dtsConfigEnvironment['default'].firebase)
  });
});
// app/adapters/application.js
define('dts/app', ['exports', 'ember', 'ember/resolver', 'ember/load-initializers', 'dts/config/environment'], function (exports, _ember, _emberResolver, _emberLoadInitializers, _dtsConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _dtsConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _dtsConfigEnvironment['default'].podModulePrefix,
    Resolver: _emberResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _dtsConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define('dts/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'dts/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _dtsConfigEnvironment) {

  var name = _dtsConfigEnvironment['default'].APP.name;
  var version = _dtsConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});
define('dts/components/convert-textarea', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		didInsertElement: function didInsertElement() {
			setTimeout(function () {
				_ember['default'].$('.converter').each(function () {
					var txet = _ember['default'].$(this).attr('data-text');
					_ember['default'].$(this).html(txet);
				});
			}, 700);
		}
	});
});
define('dts/components/credit-card-input', ['exports', 'ember-inputmask/components/credit-card-input'], function (exports, _emberInputmaskComponentsCreditCardInput) {
  exports['default'] = _emberInputmaskComponentsCreditCardInput['default'];
});
define('dts/components/currency-input', ['exports', 'ember-inputmask/components/currency-input'], function (exports, _emberInputmaskComponentsCurrencyInput) {
  exports['default'] = _emberInputmaskComponentsCurrencyInput['default'];
});
define('dts/components/date-input', ['exports', 'ember-inputmask/components/date-input'], function (exports, _emberInputmaskComponentsDateInput) {
  exports['default'] = _emberInputmaskComponentsDateInput['default'];
});
define('dts/components/email-input', ['exports', 'ember-inputmask/components/email-input'], function (exports, _emberInputmaskComponentsEmailInput) {
  exports['default'] = _emberInputmaskComponentsEmailInput['default'];
});
define('dts/components/fa-icon', ['exports', 'ember-cli-font-awesome/components/fa-icon'], function (exports, _emberCliFontAwesomeComponentsFaIcon) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCliFontAwesomeComponentsFaIcon['default'];
    }
  });
});
define('dts/components/fa-list-icon', ['exports', 'ember-cli-font-awesome/components/fa-list-icon'], function (exports, _emberCliFontAwesomeComponentsFaListIcon) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCliFontAwesomeComponentsFaListIcon['default'];
    }
  });
});
define('dts/components/fa-list', ['exports', 'ember-cli-font-awesome/components/fa-list'], function (exports, _emberCliFontAwesomeComponentsFaList) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCliFontAwesomeComponentsFaList['default'];
    }
  });
});
define('dts/components/fa-stack', ['exports', 'ember-cli-font-awesome/components/fa-stack'], function (exports, _emberCliFontAwesomeComponentsFaStack) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberCliFontAwesomeComponentsFaStack['default'];
    }
  });
});
define("dts/components/init-component", ["exports", "ember"], function (exports, _ember) {
	exports["default"] = _ember["default"].Component.extend({
		didInsertElement: function didInsertElement() {
			jQuery(".jsLeft").width(jQuery("#l-col-1").width() + "px");
			jQuery(".customscroll").height(jQuery(window).height() - 100 + "px");

			jQuery(".hiddenArea, .hiddenAreaIn").height(jQuery(window).height() - 120 + "px");
			jQuery(".jsRight").width(jQuery("#l-col-2").width() + "px");
			jQuery(".header").width(jQuery(".container").width() + "px");
			jQuery("#content").show();
			setTimeout(function () {
				$('.allreports').each(function () {
					if ($(this).attr('data-role') == 2 && $(this).attr('data-id') != $(this).attr('data-user')) {
						$(this).find('.editRepo').remove();
					} else if ($(this).attr('data-role') == 3) {
						$(this).find('.editRepo').remove();
					}
				});
			}, 500);
		}
	});
});
define('dts/components/input-mask', ['exports', 'ember-inputmask/components/input-mask'], function (exports, _emberInputmaskComponentsInputMask) {
  exports['default'] = _emberInputmaskComponentsInputMask['default'];
});
define('dts/components/message-textarea', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].TextArea.extend({
        keyDown: function keyDown(event) {
            if (event.which === 13 && !event.shiftKey) {
                // Don't insert newlines when submitting with enter
                event.preventDefault();
            }
        },

        // This next bit lets you add newlines with shift+enter without submitting
        insertNewline: function insertNewline(event) {
            if (!event.shiftKey) {
                // Do not trigger the "submit on enter" action if the user presses
                // SHIFT+ENTER, because that should just insert a new line
                this._super(event);
            }
        }
    });
});
define('dts/components/number-input', ['exports', 'ember-inputmask/components/number-input'], function (exports, _emberInputmaskComponentsNumberInput) {
  exports['default'] = _emberInputmaskComponentsNumberInput['default'];
});
define('dts/components/phone-number-input', ['exports', 'ember-inputmask/components/phone-number-input'], function (exports, _emberInputmaskComponentsPhoneNumberInput) {
  exports['default'] = _emberInputmaskComponentsPhoneNumberInput['default'];
});
define('dts/components/x-option', ['exports', 'emberx-select/components/x-option'], function (exports, _emberxSelectComponentsXOption) {
  exports['default'] = _emberxSelectComponentsXOption['default'];
});
define('dts/components/x-select', ['exports', 'emberx-select/components/x-select'], function (exports, _emberxSelectComponentsXSelect) {
  exports['default'] = _emberxSelectComponentsXSelect['default'];
});
define('dts/components/zip-code-input', ['exports', 'ember-inputmask/components/zip-code-input'], function (exports, _emberInputmaskComponentsZipCodeInput) {
  exports['default'] = _emberInputmaskComponentsZipCodeInput['default'];
});
define('dts/controllers/administrate', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Controller.extend({
		actions: {
			changeManager: function changeManager(project, manager) {
				$('#managers').modal('show');
				this.set('currentProject', project);
			},

			addDev: function addDev(project) {
				$('#developers').modal('show');
				this.set('currentProject', project);
			},

			addManager: function addManager(manager) {
				var project = this.get('currentProject');
				this.store.find('user', project.get('manager.id')).then(function (user) {
					user.get('projects').removeObject(project);
					user.save();
				});
				project.set('manager', manager);
				manager.get('projects').pushObject(project);
				project.save().then(function () {
					$('#managers').modal('hide');
					manager.save();
				});
			},

			addDevel: function addDevel(developer) {
				var project = this.get('currentProject');
				project.get('developers').pushObject(developer);
				developer.get('projects').pushObject(project);
				project.save().then(function () {
					$('#developers').modal('hide');
				}).then(function () {
					developer.save();
				});
			},
			deleteDev: function deleteDev(dev, prod) {
				dev.get('projects').removeObject(prod);
				prod.get('developers').removeObject(dev).then(function () {
					prod.save();
				}).then(function () {
					dev.save();
				});
			},
			logOut: function logOut() {
				var ref = new Firebase("https://dtsdashboard.firebaseio.com");
				ref.unauth();
				this.transitionToRoute('login');
			}
		}
	});
});
define('dts/controllers/application', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Controller.extend({
		actions: {
			logOut: function logOut() {
				var ref = new Firebase("https://dtsdashboard.firebaseio.com");
				ref.unauth();
				this.transitionToRoute('login');
			}
		}
	});
});
define('dts/controllers/array', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('dts/controllers/dashboard', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({

        actions: {
            save: function save() {
                var _this = this;
                this.set('project.isEdit', false);
                this.get('user').save().then(function () {
                    _this.get('project').save();
                });
                if (this.get('project.step') == 0) {
                    this.set('project.step', '1');
                    this.get('project').save();
                }
            },
            setStatus: function setStatus() {
                var _this = this.get('project');
                if (this.get('user.role') == 1 || this.get('user.role') == 4) {
                    $(".progress-bar").click(function (e) {
                        var width = $(this).width();
                        var offset = $(this).offset();
                        var relativeX = e.pageX - offset.left;
                        var k = relativeX * 100 / width;
                        _this.set('status', k.toFixed());
                        _this.save();
                    });
                }
            },
            editReport: function editReport(report) {
                report.set('isEdit', true);
            },
            saveReport: function saveReport(report) {
                report.set('isEdit', false);
                report.save();
            },
            newRepotr: function newRepotr() {
                var project = this.get('project');
                var report = this.store.createRecord('report', {
                    owner: this.get('user'),
                    date: new Date(),
                    body: this.get('report')
                });
                project.get('reports').pushObject(report);
                report.save().then(function () {
                    project.save();
                });
                $.ajax({
                    type: "POST",
                    url: "http://198.211.127.110/api/v1/sendMessage",
                    data: {
                        to: this.get('project.owner.mail'),
                        subject: 'New report to ' + this.get('project.title') + ', from ' + this.get('user.name') + ', DevTeam.Space',
                        body: "Hello, " + this.get('project.owner.firstName') + "%0A%0ANew report with following content was added to the project '" + this.get('project.title') + "':%0A%0A" + this.get('report') + "%0A%0ADevteam Space"
                    },
                    contentType: "application/x-www-form-urlencoded"
                });
                $('#report').modal('hide');
            },
            addLink: function addLink() {
                var project = this.get('project');
                if (project.get('step') == 1) {
                    project.set('step', 2);
                }

                var link = this.store.createRecord('link', {
                    title: this.get('linkTitle'),
                    link: this.get('linkHref')
                });
                project.get('links').pushObject(link);
                link.save().then(function () {
                    project.save();
                });
                $('#link').modal('hide');
            },

            addDate: function addDate() {
                this.set('isEstimated', true);
            },

            addBlock: function addBlock() {
                this.set('isRoad', true);
            },

            saveRoadblocks: function saveRoadblocks() {
                this.set('isRoad', false);
                this.get('project').save();
            },
            addTask: function addTask() {
                this.set('isTask', true);
            },

            saveTask: function saveTask() {
                this.set('isTask', false);
                this.get('project').save();
            },
            /*    addTask: function() {
                    var task = this.store.createRecord('task', {
                        name: this.get('tasks')
                    });
                    this.get('project.tasks').pushObject(task);
                    task.save();
                    this.get('project').save();
                    this.set('tasks', '');
                },
                 deleteTask: function(list) {
                    if (window.confirm("Элемент будет удален безвозвратно, продолжить?")) {
                        var task = this.get('project');
                        list.destroyRecord().then(function() {
                            project.save();
                        });
                    }
                },
            */
            deleteLink: function deleteLink(list) {
                var project = this.get('project');
                list.destroyRecord().then(function () {
                    project.save();
                });
            },
            compliteTask: function compliteTask(list) {
                list.set('isComplite', 'complite');
                list.save();
            },

            saveDate: function saveDate() {
                this.set('isEstimated', false);
                this.get('project').save();
            },

            cancel: function cancel() {
                this.set('project.isEdit', false);
                this.get('project').rollbackAttributes();
                this.get('user').rollbackAttributes();
            },

            edit: function edit() {
                this.set('project.isEdit', true);
            },

            startProject: function startProject() {
                var project = this.get('project');
                project.set('step', '3');
                var report = this.store.createRecord('report', {
                    owner: this.get('project.manager'),
                    isReport: true,
                    date: new Date(),
                    body: this.get('project.owner.firstName') + ', congrats on starting the project!<br/>I will take care of everything and will contact you soon.'
                });

                project.get('reports').pushObject(report);
                report.save().then(function () {
                    project.save();
                });
            },

            logOut: function logOut() {
                var ref = new Firebase("https://dtsdashboard.firebaseio.com");
                ref.unauth();
                this.transitionToRoute('login');
            }
        }
    });
});
define('dts/controllers/login', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Controller.extend({
		actions: {
			login: function login() {
				var _this = this;
				var ref = new Firebase("https://dtsdashboard.firebaseio.com");
				ref.authWithPassword({
					email: _this.get('email'),
					password: _this.get('password')
				}, function (error, authData) {
					if (error) {
						alert('Error');
					} else {
						_this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function (user) {
							var curentUser = user.get('firstObject');
							if (curentUser.get('projects.length') == 1) {
								_this.transitionToRoute('dashboard', curentUser.get('projects.firstObject'));
							} else {
								_this.transitionToRoute('projects');
							}
						});
					}
				});
			}
		}
	});
});
define('dts/controllers/object', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('dts/controllers/projects', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        actions: {
            newProject: function newProject() {
                var _this = this;
                var user = this.get('user');
                _this.store.query('user', {
                    orderBy: 'mail',
                    equalTo: 'nikita@devteam.space'
                }).then(function (users) {
                    var curentUser = users.get('firstObject');
                    var userName = user.get('name').split(' ')[0];
                    var project = _this.store.createRecord('project', {
                        owner: user,
                        step: '0',
                        date: new Date(),
                        manager: curentUser,
                        status: '0',
                        deadline: 'N/A'
                    });
                    var report = _this.store.createRecord('report', {
                        owner: curentUser,
                        date: new Date(),
                        body: 'Hi ' + userName + '!<br/>Welcome to the DevTeamSpace. We are ready to knock it out of the park for you!<br><br/>Please describe your project and add specifications below. I will be in touch with you shortly.'
                    });
                    user.get('projects').pushObject(project);
                    project.get('reports').pushObject(report);
                    project.save().then(function () {
                        user.save();
                    }).then(function () {
                        report.save();
                        _this.transitionToRoute('dashboard', project);
                    });
                });
            },

            logOut: function logOut() {
                var ref = new Firebase("https://dtsdashboard.firebaseio.com");
                ref.unauth();
                this.transitionToRoute('login');
            }
        }
    });
});
define('dts/controllers/signup', ['exports', 'ember'], function (exports, _ember) {
    exports['default'] = _ember['default'].Controller.extend({
        actions: {
            signUp: function signUp() {
                var _this = this;
                var role = this.get('role') || 3;
                var ref = new Firebase("https://dtsdashboard.firebaseio.com");
                ref.createUser({
                    email: _this.get('email'),
                    password: _this.get('password')
                }, function (error, userData) {
                    if (error) {
                        console.log("Error creating user:", error);
                    } else {
                        ref.authWithPassword({
                            email: _this.get('email'),
                            password: _this.get('password')
                        }, function (error, authData) {
                            if (error) {
                                alert('Auchh');
                            } else {
                                var user = _this.store.createRecord('user', {
                                    uid: userData.uid,
                                    mail: _this.get('email'),
                                    name: _this.get('name'),
                                    company: _this.get('company'),
                                    role: role,
                                    phone: _this.get('phone')
                                });
                                if (role == 3) {
                                    _this.store.query('user', {
                                        orderBy: 'mail',
                                        equalTo: 'nikita@devteam.space'
                                    }).then(function (users) {
                                        var curentUser = users.get('firstObject');
                                        var userName = user.get('name').split(' ')[0];
                                        var project = _this.store.createRecord('project', {
                                            owner: user,
                                            step: '0',
                                            date: new Date(),
                                            manager: users.get('firstObject'),
                                            status: '0',
                                            deadline: 'N/A'
                                        });
                                        var report = _this.store.createRecord('report', {
                                            owner: curentUser,
                                            date: new Date(),
                                            body: 'Hi ' + userName + '!<br/>Welcome to the DevTeamSpace. We are ready to knock it out of the park for you!<br><br/>Please describe your project and add specifications below. I will be in touch with you shortly.'
                                        });
                                        project.get('reports').pushObject(report);
                                        report.save().then(function () {
                                            user.get('projects').pushObject(project);
                                        }).then(function () {
                                            project.save();
                                        }).then(function () {
                                            _this.set('email', '');
                                            _this.set('name', '');
                                            _this.set('company', '');
                                            _this.set('role', '');
                                            _this.set('phone', '');
                                            user.set('image', authData.password.profileImageURL);
                                            user.save().then(function () {
                                                if (user.get('projects.length') <= 1 && user.get('role') == 3) {
                                                    _this.transitionToRoute('dashboard', user.get('projects.firstObject'));
                                                } else {
                                                    _this.transitionToRoute('projects');
                                                }
                                            });
                                        });
                                    });
                                } else {
                                    _this.set('email', '');
                                    _this.set('name', '');
                                    _this.set('company', '');
                                    _this.set('role', '');
                                    _this.set('phone', '');
                                    user.set('image', authData.password.profileImageURL);
                                    user.save().then(function () {
                                        if (user.get('projects.length') <= 1 && user.get('role') == 3) {
                                            _this.transitionToRoute('dashboard', user.get('projects.firstObject'));
                                        } else {
                                            _this.transitionToRoute('projects');
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        }
    });
});
define('dts/helpers/moment-calendar', ['exports', 'ember-moment/helpers/moment-calendar'], function (exports, _emberMomentHelpersMomentCalendar) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberMomentHelpersMomentCalendar['default'];
    }
  });
  Object.defineProperty(exports, 'momentCalendar', {
    enumerable: true,
    get: function get() {
      return _emberMomentHelpersMomentCalendar.momentCalendar;
    }
  });
});
define('dts/helpers/moment-duration', ['exports', 'ember-moment/helpers/moment-duration'], function (exports, _emberMomentHelpersMomentDuration) {
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function get() {
      return _emberMomentHelpersMomentDuration['default'];
    }
  });
});
define('dts/helpers/moment-format', ['exports', 'ember', 'dts/config/environment', 'ember-moment/helpers/moment-format'], function (exports, _ember, _dtsConfigEnvironment, _emberMomentHelpersMomentFormat) {
  exports['default'] = _emberMomentHelpersMomentFormat['default'].extend({
    globalAllowEmpty: !!_ember['default'].get(_dtsConfigEnvironment['default'], 'moment.allowEmpty')
  });
});
define('dts/helpers/moment-from-now', ['exports', 'ember', 'dts/config/environment', 'ember-moment/helpers/moment-from-now'], function (exports, _ember, _dtsConfigEnvironment, _emberMomentHelpersMomentFromNow) {
  exports['default'] = _emberMomentHelpersMomentFromNow['default'].extend({
    globalAllowEmpty: !!_ember['default'].get(_dtsConfigEnvironment['default'], 'moment.allowEmpty')
  });
});
define('dts/helpers/moment-to-now', ['exports', 'ember', 'dts/config/environment', 'ember-moment/helpers/moment-to-now'], function (exports, _ember, _dtsConfigEnvironment, _emberMomentHelpersMomentToNow) {
  exports['default'] = _emberMomentHelpersMomentToNow['default'].extend({
    globalAllowEmpty: !!_ember['default'].get(_dtsConfigEnvironment['default'], 'moment.allowEmpty')
  });
});
define('dts/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'dts/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _dtsConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_dtsConfigEnvironment['default'].APP.name, _dtsConfigEnvironment['default'].APP.version)
  };
});
define('dts/initializers/emberfire', ['exports', 'emberfire/initializers/emberfire'], function (exports, _emberfireInitializersEmberfire) {
  exports['default'] = _emberfireInitializersEmberfire['default'];
});
define('dts/initializers/export-application-global', ['exports', 'ember', 'dts/config/environment'], function (exports, _ember, _dtsConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_dtsConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _dtsConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_dtsConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('dts/initializers/initialize-torii-callback', ['exports', 'torii/redirect-handler'], function (exports, _toriiRedirectHandler) {
  exports['default'] = {
    name: 'torii-callback',
    before: 'torii',
    initialize: function initialize(application) {
      if (arguments[1]) {
        // Ember < 2.1
        application = arguments[1];
      }
      application.deferReadiness();
      _toriiRedirectHandler['default'].handle(window)['catch'](function () {
        application.advanceReadiness();
      });
    }
  };
});
define('dts/initializers/initialize-torii-session', ['exports', 'torii/configuration', 'torii/bootstrap/session'], function (exports, _toriiConfiguration, _toriiBootstrapSession) {
  exports['default'] = {
    name: 'torii-session',
    after: 'torii',

    initialize: function initialize(application) {
      if (arguments[1]) {
        // Ember < 2.1
        application = arguments[1];
      }
      if (_toriiConfiguration['default'].sessionServiceName) {
        (0, _toriiBootstrapSession['default'])(application, _toriiConfiguration['default'].sessionServiceName);

        var sessionFactoryName = 'service:' + _toriiConfiguration['default'].sessionServiceName;
        application.inject('adapter', _toriiConfiguration['default'].sessionServiceName, sessionFactoryName);
      }
    }
  };
});
define('dts/initializers/initialize-torii', ['exports', 'torii/bootstrap/torii', 'torii/configuration'], function (exports, _toriiBootstrapTorii, _toriiConfiguration) {

  var initializer = {
    name: 'torii',
    initialize: function initialize(application) {
      if (arguments[1]) {
        // Ember < 2.1
        application = arguments[1];
      }
      (0, _toriiBootstrapTorii['default'])(application);
      application.inject('route', 'torii', 'service:torii');
    }
  };

  if (window.DS) {
    initializer.after = 'store';
  }

  exports['default'] = initializer;
});
define('dts/instance-initializers/setup-routes', ['exports', 'torii/configuration', 'torii/bootstrap/routing', 'torii/router-dsl-ext'], function (exports, _toriiConfiguration, _toriiBootstrapRouting, _toriiRouterDslExt) {
  exports['default'] = {
    name: 'torii-setup-routes',
    initialize: function initialize(applicationInstance, registry) {
      if (_toriiConfiguration['default'].sessionServiceName) {
        var router = applicationInstance.get('router');
        var setupRoutes = function setupRoutes() {
          var authenticatedRoutes = router.router.authenticatedRoutes;
          var hasAuthenticatedRoutes = !Ember.isEmpty(authenticatedRoutes);
          if (hasAuthenticatedRoutes) {
            (0, _toriiBootstrapRouting['default'])(applicationInstance, authenticatedRoutes);
          }
          router.off('willTransition', setupRoutes);
        };
        router.on('willTransition', setupRoutes);
      }
    }
  };
});
define('dts/instance-initializers/walk-providers', ['exports', 'torii/configuration', 'torii/lib/container-utils'], function (exports, _toriiConfiguration, _toriiLibContainerUtils) {
  exports['default'] = {
    name: 'torii-walk-providers',
    initialize: function initialize(applicationInstance) {
      // Walk all configured providers and eagerly instantiate
      // them. This gives providers with initialization side effects
      // like facebook-connect a chance to load up assets.
      for (var key in _toriiConfiguration['default'].providers) {
        if (_toriiConfiguration['default'].providers.hasOwnProperty(key)) {
          (0, _toriiLibContainerUtils.lookup)(applicationInstance, 'torii-provider:' + key);
        }
      }
    }
  };
});
define('dts/models/link', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    title: _emberData['default'].attr('string'),
    link: _emberData['default'].attr('string')
  });
});
define('dts/models/project', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    owner: _emberData['default'].belongsTo('user', { inverse: null }),
    title: _emberData['default'].attr('string'),
    description: _emberData['default'].attr('string'),
    manager: _emberData['default'].belongsTo('user', { inverse: null }),
    developers: _emberData['default'].hasMany('user', { inverse: null }),
    reports: _emberData['default'].hasMany('report'),
    links: _emberData['default'].hasMany('link'),
    status: _emberData['default'].attr('string'),
    deadline: _emberData['default'].attr('string'),
    date: _emberData['default'].attr('date'),
    roadblocks: _emberData['default'].attr('string'),
    step: _emberData['default'].attr('number'),
    tasks: _emberData['default'].attr('string'),

    sortedItems: (function () {
      var items = this.get('reports').toArray();
      return items.sort(function (lhs, rhs) {
        return rhs.get('date') - lhs.get('date');
      });
    }).property('reports.@each.date'),

    stratProjectBut: (function () {
      if (this.get('step') != 3 && this.get('step') != 0) {
        return true;
      } else {
        return false;
      }
    }).property('step'),

    isNew: (function () {
      return this.get('step') == 0 && !this.get('title') ? true : false;
    }).property('title'),

    isEdit: (function () {
      return this.get('step') == 0 ? true : false;
    }).property('step'),

    isLinks: (function () {
      return this.get('step') == 1 ? true : false;
    }).property('step'),

    isReady: (function () {
      return this.get('step') == 2 ? true : false;
    }).property('step'),

    isEmptyDevelopers: (function () {
      return this.get('developers.length') == 0 ? true : false;
    }).property('developers'),

    statusField: (function () {
      if (this.get('stratProjectBut')) {
        return this.get('status') + '%, Not Started';
      } else if (this.get('status') == 100) {
        return this.get('status') + '%, Finished';
      } else {
        return this.get('status') + '%, In progress';
      }
    }).property('status'),

    blockers: (function () {
      return this.get('roadblocks') ? 'warning' : 'btn-default';
    }).property('roadblocks')

  });
});
define('dts/models/report', ['exports', 'ember-data', 'moment'], function (exports, _emberData, _moment) {
  exports['default'] = _emberData['default'].Model.extend({
    body: _emberData['default'].attr('string'),
    isReport: _emberData['default'].attr('boolean'),
    date: _emberData['default'].attr('date'),
    owner: _emberData['default'].belongsTo('user'),
    formattedDate: (function () {
      var date = this.get('date');
      return (0, _moment['default'])(date).format('LL');
    }).property('date')
  });
});
define('dts/models/task', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    name: _emberData['default'].attr('string'),
    isComplite: _emberData['default'].attr('string')
  });
});
define('dts/models/user', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    uid: _emberData['default'].attr('string'),
    image: _emberData['default'].attr('string'),
    projects: _emberData['default'].hasMany('project', { inverse: null }),
    mail: _emberData['default'].attr('string'),
    name: _emberData['default'].attr('string'),
    company: _emberData['default'].attr('string'),
    role: _emberData['default'].attr('number'),
    мanaged: _emberData['default'].attr('number'),
    сompleted: _emberData['default'].attr('number'),
    phone: _emberData['default'].attr('string'),
    isSuperAdmin: (function () {
      return this.get('role') == 4 ? true : false;
    }).property('role'),
    isCustomer: (function () {
      return this.get('role') == 3 ? true : false;
    }).property('role'),
    isManager: (function () {
      return this.get('role') == 1 ? true : false;
    }).property('role'),
    isDeveloper: (function () {
      return this.get('role') == 2 ? true : false;
    }).property('role'),
    length: (function () {
      return this.get('projects.length');
    }).property('projects.length'),
    sortedProjects: (function () {
      var items = this.get('projects').toArray();
      return items.sort(function (lhs, rhs) {
        return rhs.get('date') - lhs.get('date');
      });
    }).property('projects.@each.date'),
    firstName: (function () {
      return this.get('name').split(' ')[0];
    }).property('user.name')
  });
});
define('dts/router', ['exports', 'ember', 'dts/config/environment'], function (exports, _ember, _dtsConfigEnvironment) {

  var Router = _ember['default'].Router.extend({
    location: _dtsConfigEnvironment['default'].locationType
  });

  Router.map(function () {
    this.route('projects');
    this.route('dashboard', { path: "dashboard/:project_id" });
    this.route('login');
    this.route('signup');
    this.route('administrate');
  });

  exports['default'] = Router;
});
define('dts/routes/administrate', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({

		setupController: function setupController(controller, model) {

			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			var authData = ref.getAuth();
			var _this = this;

			if (authData) {
				this.store.query('user', { orderBy: 'role', equalTo: 3 }).then(function (users1) {
					controller.set('customers', users1);
				});
				this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function (user) {
					var curentUser = user.get('firstObject');
					if (!curentUser.get('isSuperAdmin')) {
						_this.transitionTo('projects');
					}
				});
				this.store.query('user', { orderBy: 'role', equalTo: 1 }).then(function (users2) {
					controller.set('managers', users2);
				});
				this.store.query('user', { orderBy: 'role', equalTo: 2 }).then(function (users3) {
					controller.set('developers', users3);
				});
			} else {
				this.transitionTo('login');
			}
		}

	});
});
define('dts/routes/dashboard', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({

		model: function model(params) {
			return this.store.find('project', params.project_id);
		},

		setupController: function setupController(controller, model) {

			controller.set('project', model);

			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			var authData = ref.getAuth();

			if (authData) {
				this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function (users) {
					controller.set('user', users.get('firstObject'));
				});
			} else {
				this.transitionTo('login');
			}
		}

	});
});
define('dts/routes/index', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({
		setupController: function setupController(controller, model) {
			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			var authData = ref.getAuth();
			controller.set('authData', authData);
			this.transitionTo('projects');
		}
	});
});
define('dts/routes/login', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({
		setupController: function setupController(controller, model) {
			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			var authData = ref.getAuth();
			if (authData) {
				this.transitionTo('projects');
			}
		}
	});
});
define('dts/routes/projects', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({

		setupController: function setupController(controller, model) {
			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			var authData = ref.getAuth();
			var _this = this;
			if (authData) {
				this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function (user) {
					var curentUser = user.get('firstObject');
					controller.set('user', curentUser);
				});
			} else {
				this.transitionTo('login');
			}
		}
	});
});
define('dts/routes/signup', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('dts/serializers/project', ['exports', 'emberfire/serializers/firebase'], function (exports, _emberfireSerializersFirebase) {
  exports['default'] = _emberfireSerializersFirebase['default'].extend({});
});
define('dts/serializers/user', ['exports', 'emberfire/serializers/firebase'], function (exports, _emberfireSerializersFirebase) {
  exports['default'] = _emberfireSerializersFirebase['default'].extend({});
});
define('dts/services/firebase', ['exports', 'emberfire/services/firebase', 'dts/config/environment'], function (exports, _emberfireServicesFirebase, _dtsConfigEnvironment) {

  _emberfireServicesFirebase['default'].config = _dtsConfigEnvironment['default'];

  exports['default'] = _emberfireServicesFirebase['default'];
});
define('dts/services/moment', ['exports', 'ember', 'dts/config/environment', 'ember-moment/services/moment'], function (exports, _ember, _dtsConfigEnvironment, _emberMomentServicesMoment) {
  exports['default'] = _emberMomentServicesMoment['default'].extend({
    defaultFormat: _ember['default'].get(_dtsConfigEnvironment['default'], 'moment.outputFormat')
  });
});
define("dts/templates/administrate", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 6,
              "column": 12
            },
            "end": {
              "line": 6,
              "column": 69
            }
          },
          "moduleName": "dts/templates/administrate.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("DevTeam.Space");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 9,
              "column": 12
            },
            "end": {
              "line": 9,
              "column": 61
            }
          },
          "moduleName": "dts/templates/administrate.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Projects");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child2 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "fragmentReason": false,
              "revision": "Ember@2.2.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 23,
                  "column": 7
                },
                "end": {
                  "line": 25,
                  "column": 4
                }
              },
              "moduleName": "dts/templates/administrate.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("					");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("strong");
              var el2 = dom.createTextNode("Project - ");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode(" ");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n				");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
              return morphs;
            },
            statements: [["content", "project.title", ["loc", [null, [24, 23], [24, 40]]]]],
            locals: [],
            templates: []
          };
        })();
        var child1 = (function () {
          return {
            meta: {
              "fragmentReason": false,
              "revision": "Ember@2.2.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 36,
                  "column": 14
                },
                "end": {
                  "line": 38,
                  "column": 14
                }
              },
              "moduleName": "dts/templates/administrate.hbs"
            },
            isEmpty: false,
            arity: 1,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("		            	");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("span");
              var el2 = dom.createElement("img");
              dom.setAttribute(el2, "alt", "");
              dom.appendChild(el1, el2);
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode(" ");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("i");
              dom.setAttribute(el2, "class", "fa fa-icon fa-trash pointer");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var element4 = dom.childAt(fragment, [1]);
              var element5 = dom.childAt(element4, [0]);
              var element6 = dom.childAt(element4, [3]);
              var morphs = new Array(3);
              morphs[0] = dom.createAttrMorph(element5, 'src');
              morphs[1] = dom.createMorphAt(element4, 1, 1);
              morphs[2] = dom.createElementMorph(element6);
              return morphs;
            },
            statements: [["attribute", "src", ["concat", [["get", "dev.image", ["loc", [null, [37, 33], [37, 42]]]]]]], ["content", "dev.name", ["loc", [null, [37, 53], [37, 65]]]], ["element", "action", ["deleteDev", ["get", "dev", ["loc", [null, [37, 90], [37, 93]]]], ["get", "project", ["loc", [null, [37, 94], [37, 101]]]]], [], ["loc", [null, [37, 69], [37, 103]]]]],
            locals: ["dev"],
            templates: []
          };
        })();
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 19,
                "column": 1
              },
              "end": {
                "line": 43,
                "column": 1
              }
            },
            "moduleName": "dts/templates/administrate.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "col-md-12 feed-bg adm ");
            var el2 = dom.createTextNode("\n    	");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "row");
            var el3 = dom.createTextNode("\n    		");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("div");
            dom.setAttribute(el3, "class", "col-xs-3");
            var el4 = dom.createTextNode("\n");
            dom.appendChild(el3, el4);
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("br");
            dom.appendChild(el3, el4);
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("br");
            dom.appendChild(el3, el4);
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("span");
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n    		");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n    		");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("div");
            dom.setAttribute(el3, "class", "col-xs-3");
            var el4 = dom.createTextNode("\n    			");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("p");
            var el5 = dom.createTextNode("Manager ");
            dom.appendChild(el4, el5);
            var el5 = dom.createComment("");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n    			");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("p");
            dom.setAttribute(el4, "class", "dev-team__item text-center pointer");
            var el5 = dom.createTextNode("\n	                ");
            dom.appendChild(el4, el5);
            var el5 = dom.createElement("img");
            dom.setAttribute(el5, "alt", "");
            dom.appendChild(el4, el5);
            var el5 = dom.createTextNode("\n	            ");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n    		");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n    		");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("div");
            dom.setAttribute(el3, "class", "col-xs-6");
            var el4 = dom.createTextNode("\n    			");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("p");
            var el5 = dom.createTextNode("Developers ");
            dom.appendChild(el4, el5);
            var el5 = dom.createElement("span");
            dom.setAttribute(el5, "class", "fa fa-icon fa-plus pointer");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n				");
            dom.appendChild(el3, el4);
            var el4 = dom.createElement("div");
            dom.setAttribute(el4, "class", "dev-team__item");
            var el5 = dom.createTextNode("\n");
            dom.appendChild(el4, el5);
            var el5 = dom.createComment("");
            dom.appendChild(el4, el5);
            var el5 = dom.createTextNode("	            ");
            dom.appendChild(el4, el5);
            dom.appendChild(el3, el4);
            var el4 = dom.createTextNode("\n    		");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n    	");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n	");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element7 = dom.childAt(fragment, [1, 1]);
            var element8 = dom.childAt(element7, [1]);
            var element9 = dom.childAt(element8, [6]);
            var element10 = dom.childAt(element7, [3]);
            var element11 = dom.childAt(element10, [3, 1]);
            var element12 = dom.childAt(element7, [5]);
            var element13 = dom.childAt(element12, [1, 1]);
            var morphs = new Array(9);
            morphs[0] = dom.createMorphAt(element8, 1, 1);
            morphs[1] = dom.createMorphAt(element8, 3, 3);
            morphs[2] = dom.createMorphAt(element8, 5, 5);
            morphs[3] = dom.createAttrMorph(element9, 'class');
            morphs[4] = dom.createMorphAt(dom.childAt(element10, [1]), 1, 1);
            morphs[5] = dom.createAttrMorph(element11, 'src');
            morphs[6] = dom.createElementMorph(element11);
            morphs[7] = dom.createElementMorph(element13);
            morphs[8] = dom.createMorphAt(dom.childAt(element12, [3]), 1, 1);
            return morphs;
          },
          statements: [["block", "link-to", ["dashboard", ["get", "project", ["loc", [null, [23, 30], [23, 37]]]]], [], 0, null, ["loc", [null, [23, 7], [25, 16]]]], ["content", "customer.name", ["loc", [null, [25, 21], [25, 38]]]], ["content", "project.statusField", ["loc", [null, [25, 43], [25, 66]]]], ["attribute", "class", ["concat", ["progectStatus ", ["get", "project.blockers", ["loc", [null, [25, 95], [25, 111]]]]]]], ["content", "project.manager.name", ["loc", [null, [28, 18], [28, 42]]]], ["attribute", "src", ["concat", [["get", "project.manager.image", ["loc", [null, [30, 80], [30, 101]]]]]]], ["element", "action", ["changeManager", ["get", "project", ["loc", [null, [30, 47], [30, 54]]]], ["get", "project.manager", ["loc", [null, [30, 55], [30, 70]]]]], [], ["loc", [null, [30, 22], [30, 72]]]], ["element", "action", ["addDev", ["get", "project", ["loc", [null, [34, 45], [34, 52]]]]], [], ["loc", [null, [34, 27], [34, 54]]]], ["block", "each", [["get", "project.developers", ["loc", [null, [36, 22], [36, 40]]]]], [], 1, null, ["loc", [null, [36, 14], [38, 23]]]]],
          locals: ["project"],
          templates: [child0, child1]
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 17,
              "column": 0
            },
            "end": {
              "line": 44,
              "column": 0
            }
          },
          "moduleName": "dts/templates/administrate.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("	\n");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "each", [["get", "customer.projects", ["loc", [null, [19, 9], [19, 26]]]]], [], 0, null, ["loc", [null, [19, 1], [43, 10]]]]],
        locals: ["customer"],
        templates: [child0]
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 51,
              "column": 5
            },
            "end": {
              "line": 53,
              "column": 5
            }
          },
          "moduleName": "dts/templates/administrate.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("						");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("li");
          dom.setAttribute(el1, "class", "pointer dev-team__item");
          var el2 = dom.createElement("img");
          dom.setAttribute(el2, "alt", "");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element2 = dom.childAt(fragment, [1]);
          var element3 = dom.childAt(element2, [0]);
          var morphs = new Array(3);
          morphs[0] = dom.createElementMorph(element2);
          morphs[1] = dom.createAttrMorph(element3, 'src');
          morphs[2] = dom.createMorphAt(element2, 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["addManager", ["get", "manager", ["loc", [null, [52, 32], [52, 39]]]]], [], ["loc", [null, [52, 10], [52, 41]]]], ["attribute", "src", ["concat", [["get", "manager.image", ["loc", [null, [52, 85], [52, 98]]]]]]], ["content", "manager.name", ["loc", [null, [52, 109], [52, 125]]]]],
        locals: ["manager"],
        templates: []
      };
    })();
    var child4 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 61,
              "column": 5
            },
            "end": {
              "line": 63,
              "column": 5
            }
          },
          "moduleName": "dts/templates/administrate.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("						");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("li");
          dom.setAttribute(el1, "class", "pointer dev-team__item");
          var el2 = dom.createElement("img");
          dom.setAttribute(el2, "alt", "");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var element1 = dom.childAt(element0, [0]);
          var morphs = new Array(3);
          morphs[0] = dom.createElementMorph(element0);
          morphs[1] = dom.createAttrMorph(element1, 'src');
          morphs[2] = dom.createMorphAt(element0, 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["addDevel", ["get", "developer", ["loc", [null, [62, 30], [62, 39]]]]], [], ["loc", [null, [62, 10], [62, 41]]]], ["attribute", "src", ["concat", [["get", "developer.image", ["loc", [null, [62, 85], [62, 100]]]]]]], ["content", "developer.name", ["loc", [null, [62, 111], [62, 129]]]]],
        locals: ["developer"],
        templates: []
      };
    })();
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type", "multiple-nodes"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 67,
            "column": 0
          }
        },
        "moduleName": "dts/templates/administrate.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "class", "header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-10");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-2 text-right");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode(" | ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("a");
        dom.setAttribute(el4, "class", "underline");
        var el5 = dom.createTextNode("Logout");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "hiddenArea");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "hiddenAreaIn");
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "row");
        var el4 = dom.createTextNode("\n");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "modal fade");
        dom.setAttribute(el1, "id", "managers");
        dom.setAttribute(el1, "tabindex", "-1");
        dom.setAttribute(el1, "role", "dialog");
        dom.setAttribute(el1, "aria-hidden", "true");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "modal-dialog");
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "modal-content");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-header");
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "close");
        dom.setAttribute(el5, "data-dismiss", "modal");
        dom.setAttribute(el5, "aria-hidden", "true");
        var el6 = dom.createTextNode("×");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("h4");
        dom.setAttribute(el5, "class", "modal-title");
        var el6 = dom.createTextNode("Add Manager");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-body");
        dom.setAttribute(el4, "ng-transclude", "");
        var el5 = dom.createTextNode("\n                  ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("ul");
        dom.setAttribute(el5, "class", "managers");
        var el6 = dom.createTextNode("\n");
        dom.appendChild(el5, el6);
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("					");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n   ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "modal fade");
        dom.setAttribute(el1, "id", "developers");
        dom.setAttribute(el1, "tabindex", "-1");
        dom.setAttribute(el1, "role", "dialog");
        dom.setAttribute(el1, "aria-hidden", "true");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "modal-dialog");
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "modal-content");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-header");
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "close");
        dom.setAttribute(el5, "data-dismiss", "modal");
        dom.setAttribute(el5, "aria-hidden", "true");
        var el6 = dom.createTextNode("×");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("h4");
        dom.setAttribute(el5, "class", "modal-title");
        var el6 = dom.createTextNode("Add Developer");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-body");
        dom.setAttribute(el4, "ng-transclude", "");
        var el5 = dom.createTextNode("\n                  ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("ul");
        dom.setAttribute(el5, "class", "managers");
        var el6 = dom.createTextNode("\n");
        dom.appendChild(el5, el6);
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("					");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n   ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element14 = dom.childAt(fragment, [3, 1]);
        var element15 = dom.childAt(element14, [3]);
        var element16 = dom.childAt(element15, [3]);
        var morphs = new Array(7);
        morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
        morphs[1] = dom.createMorphAt(dom.childAt(element14, [1]), 1, 1);
        morphs[2] = dom.createMorphAt(element15, 1, 1);
        morphs[3] = dom.createElementMorph(element16);
        morphs[4] = dom.createMorphAt(dom.childAt(fragment, [5, 1, 1]), 1, 1);
        morphs[5] = dom.createMorphAt(dom.childAt(fragment, [7, 1, 0, 1, 1]), 1, 1);
        morphs[6] = dom.createMorphAt(dom.childAt(fragment, [9, 1, 0, 1, 1]), 1, 1);
        return morphs;
      },
      statements: [["content", "init-component", ["loc", [null, [2, 0], [2, 18]]]], ["block", "link-to", ["projects"], ["class", "header__logo"], 0, null, ["loc", [null, [6, 12], [6, 81]]]], ["block", "link-to", ["projects"], ["class", "underline"], 1, null, ["loc", [null, [9, 12], [9, 73]]]], ["element", "action", ["logOut"], [], ["loc", [null, [9, 79], [9, 98]]]], ["block", "each", [["get", "customers", ["loc", [null, [17, 8], [17, 17]]]]], [], 2, null, ["loc", [null, [17, 0], [44, 9]]]], ["block", "each", [["get", "managers", ["loc", [null, [51, 13], [51, 21]]]]], [], 3, null, ["loc", [null, [51, 5], [53, 14]]]], ["block", "each", [["get", "developers", ["loc", [null, [61, 13], [61, 23]]]]], [], 4, null, ["loc", [null, [61, 5], [63, 14]]]]],
      locals: [],
      templates: [child0, child1, child2, child3, child4]
    };
  })());
});
define("dts/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["multiple-nodes", "wrong-type"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 10,
            "column": 0
          }
        },
        "moduleName": "dts/templates/application.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "container");
        dom.setAttribute(el1, "id", "content");
        dom.setAttribute(el1, "style", "margin-top:20px;");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "slide-animation");
        var el3 = dom.createTextNode("\n		");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "container");
        var el4 = dom.createTextNode("\n		   ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0, 1, 1]), 1, 1);
        morphs[1] = dom.createMorphAt(fragment, 2, 2, contextualElement);
        return morphs;
      },
      statements: [["content", "outlet", ["loc", [null, [4, 5], [4, 15]]]], ["inline", "outlet", ["modal"], [], ["loc", [null, [8, 0], [8, 18]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/components/convert-textarea", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "triple-curlies"
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 51
          }
        },
        "moduleName": "dts/templates/components/convert-textarea.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "converter");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var morphs = new Array(1);
        morphs[0] = dom.createAttrMorph(element0, 'data-text');
        return morphs;
      },
      statements: [["attribute", "data-text", ["concat", [["get", "value", ["loc", [null, [1, 36], [1, 41]]]]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/components/init-component", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "dts/templates/components/init-component.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/components/message-textarea", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["empty-body"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 0
          }
        },
        "moduleName": "dts/templates/components/message-textarea.hbs"
      },
      isEmpty: true,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/components/modal-window", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "triple-curlies"
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 18,
            "column": 0
          }
        },
        "moduleName": "dts/templates/components/modal-window.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "modal fade in ");
        dom.setAttribute(el1, "style", "display: block");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "modal-dialog modal-sm");
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "modal-content");
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-header");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "close");
        dom.setAttribute(el5, "data-dismiss", "modal");
        dom.setAttribute(el5, "aria-hidden", "true");
        var el6 = dom.createTextNode("×");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("h4");
        dom.setAttribute(el5, "class", "modal-title");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-body");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n      ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-footer");
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "btn btn-default");
        dom.setAttribute(el5, "data-dismiss", "modal");
        var el6 = dom.createTextNode("Закрыть");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n        ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "btn btn-success");
        var el6 = dom.createTextNode("Сохранить");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n      ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n    ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n  ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1, 1]);
        var element1 = dom.childAt(element0, [5, 3]);
        var morphs = new Array(3);
        morphs[0] = dom.createMorphAt(dom.childAt(element0, [1, 3]), 0, 0);
        morphs[1] = dom.createMorphAt(dom.childAt(element0, [3]), 1, 1);
        morphs[2] = dom.createElementMorph(element1);
        return morphs;
      },
      statements: [["content", "title", ["loc", [null, [6, 32], [6, 41]]]], ["content", "yield", ["loc", [null, [9, 8], [9, 17]]]], ["element", "action", ["ok"], [], ["loc", [null, [13, 54], [13, 69]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/components/x-select", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "dts/templates/components/x-select.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["content", "yield", ["loc", [null, [1, 0], [1, 9]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/dashboard", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 6,
              "column": 12
            },
            "end": {
              "line": 6,
              "column": 69
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("DevTeam.Space");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 9,
                "column": 38
              },
              "end": {
                "line": 9,
                "column": 95
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Administrate");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 9,
              "column": 12
            },
            "end": {
              "line": 9,
              "column": 110
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" | ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["block", "link-to", ["administrate"], ["class", "underline"], 0, null, ["loc", [null, [9, 38], [9, 107]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 9,
              "column": 118
            },
            "end": {
              "line": 9,
              "column": 167
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Projects");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 21,
              "column": 33
            },
            "end": {
              "line": 28,
              "column": 33
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                                    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "pulsar");
          var el2 = dom.createTextNode("\n                                       ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "ring");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                       ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "ring");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                       ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "ring");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                       ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "ring");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child4 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 29,
              "column": 32
            },
            "end": {
              "line": 53,
              "column": 32
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "row");
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "col-xs-12 col-md-8");
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                    ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "col-xs-12 col-md-4");
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                    ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "btn btn-info");
          var el2 = dom.createTextNode("Save Project");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "btn btn-warning");
          var el2 = dom.createTextNode("Cancel");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element21 = dom.childAt(fragment, [3]);
          var element22 = dom.childAt(element21, [3]);
          var element23 = dom.childAt(fragment, [5]);
          var element24 = dom.childAt(fragment, [7]);
          var morphs = new Array(7);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          morphs[1] = dom.createMorphAt(dom.childAt(element21, [1, 1]), 1, 1);
          morphs[2] = dom.createMorphAt(dom.childAt(element22, [1]), 1, 1);
          morphs[3] = dom.createMorphAt(dom.childAt(element22, [3]), 1, 1);
          morphs[4] = dom.createMorphAt(dom.childAt(element22, [5]), 1, 1);
          morphs[5] = dom.createElementMorph(element23);
          morphs[6] = dom.createElementMorph(element24);
          return morphs;
        },
        statements: [["inline", "input", [], ["maxlength", "40", "value", ["subexpr", "@mut", [["get", "project.title", ["loc", [null, [31, 65], [31, 78]]]]], [], []], "class", "form-control", "placeholder", "Enter your project name"], ["loc", [null, [31, 36], [31, 139]]]], ["inline", "textarea", [], ["rows", "6", "cols", "45", "value", ["subexpr", "@mut", [["get", "project.description", ["loc", [null, [36, 80], [36, 99]]]]], [], []], "class", "form-control", "placeholder", "Enter project description"], ["loc", [null, [36, 44], [36, 162]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "project.owner.mail", ["loc", [null, [41, 58], [41, 76]]]]], [], []], "class", "form-control", "placeholder", "Your email"], ["loc", [null, [41, 44], [41, 124]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "project.owner.name", ["loc", [null, [44, 58], [44, 76]]]]], [], []], "class", "form-control", "placeholder", "Your name"], ["loc", [null, [44, 44], [44, 123]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "project.owner.phone", ["loc", [null, [47, 58], [47, 77]]]]], [], []], "class", "form-control", "placeholder", "Your phone"], ["loc", [null, [47, 44], [47, 125]]]], ["element", "action", ["save"], [], ["loc", [null, [51, 40], [51, 57]]]], ["element", "action", ["cancel"], [], ["loc", [null, [52, 40], [52, 59]]]]],
        locals: [],
        templates: []
      };
    })();
    var child5 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          var child0 = (function () {
            return {
              meta: {
                "fragmentReason": false,
                "revision": "Ember@2.2.0",
                "loc": {
                  "source": null,
                  "start": {
                    "line": 78,
                    "column": 40
                  },
                  "end": {
                    "line": 80,
                    "column": 40
                  }
                },
                "moduleName": "dts/templates/dashboard.hbs"
              },
              isEmpty: false,
              arity: 0,
              cachedFragment: null,
              hasRendered: false,
              buildFragment: function buildFragment(dom) {
                var el0 = dom.createDocumentFragment();
                var el1 = dom.createTextNode("                                        ");
                dom.appendChild(el0, el1);
                var el1 = dom.createElement("button");
                dom.setAttribute(el1, "style", "position: relative");
                dom.setAttribute(el1, "class", "btn btn-success");
                var el2 = dom.createTextNode("Start Project");
                dom.appendChild(el1, el2);
                dom.appendChild(el0, el1);
                var el1 = dom.createTextNode("\n");
                dom.appendChild(el0, el1);
                return el0;
              },
              buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
                var element17 = dom.childAt(fragment, [1]);
                var morphs = new Array(1);
                morphs[0] = dom.createElementMorph(element17);
                return morphs;
              },
              statements: [["element", "action", ["startProject"], [], ["loc", [null, [79, 48], [79, 73]]]]],
              locals: [],
              templates: []
            };
          })();
          return {
            meta: {
              "fragmentReason": false,
              "revision": "Ember@2.2.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 77,
                  "column": 36
                },
                "end": {
                  "line": 81,
                  "column": 36
                }
              },
              "moduleName": "dts/templates/dashboard.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createComment("");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(1);
              morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
              dom.insertBoundary(fragment, 0);
              dom.insertBoundary(fragment, null);
              return morphs;
            },
            statements: [["block", "unless", [["get", "user.isCustomer", ["loc", [null, [78, 50], [78, 65]]]]], [], 0, null, ["loc", [null, [78, 40], [80, 51]]]]],
            locals: [],
            templates: [child0]
          };
        })();
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 75,
                "column": 32
              },
              "end": {
                "line": 82,
                "column": 32
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                                    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("button");
            dom.setAttribute(el1, "class", "btn btn-info");
            var el2 = dom.createTextNode("Edit Project");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element18 = dom.childAt(fragment, [1]);
            var morphs = new Array(2);
            morphs[0] = dom.createElementMorph(element18);
            morphs[1] = dom.createMorphAt(fragment, 3, 3, contextualElement);
            dom.insertBoundary(fragment, null);
            return morphs;
          },
          statements: [["element", "action", ["edit"], [], ["loc", [null, [76, 44], [76, 61]]]], ["block", "if", [["get", "project.stratProjectBut", ["loc", [null, [77, 42], [77, 65]]]]], [], 0, null, ["loc", [null, [77, 36], [81, 43]]]]],
          locals: [],
          templates: [child0]
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 53,
              "column": 32
            },
            "end": {
              "line": 83,
              "column": 32
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("h4");
          dom.setAttribute(el2, "class", "media-heading");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "row");
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "col-xs-12 col-md-8");
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                    ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("div");
          dom.setAttribute(el2, "class", "col-xs-12 col-md-4");
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group elip");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group elip");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                        ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("div");
          dom.setAttribute(el3, "class", "form-group elip");
          var el4 = dom.createTextNode("\n                                            ");
          dom.appendChild(el3, el4);
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          var el4 = dom.createTextNode("\n                                        ");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                                    ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element19 = dom.childAt(fragment, [3]);
          var element20 = dom.childAt(element19, [3]);
          var morphs = new Array(6);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 1]), 0, 0);
          morphs[1] = dom.createMorphAt(dom.childAt(element19, [1, 1]), 1, 1);
          morphs[2] = dom.createMorphAt(dom.childAt(element20, [1]), 1, 1);
          morphs[3] = dom.createMorphAt(dom.childAt(element20, [3]), 1, 1);
          morphs[4] = dom.createMorphAt(dom.childAt(element20, [5]), 1, 1);
          morphs[5] = dom.createMorphAt(fragment, 5, 5, contextualElement);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["content", "project.title", ["loc", [null, [55, 62], [55, 79]]]], ["inline", "convert-textarea", [], ["value", ["subexpr", "@mut", [["get", "project.description", ["loc", [null, [60, 69], [60, 88]]]]], [], []]], ["loc", [null, [60, 44], [60, 90]]]], ["content", "project.owner.mail", ["loc", [null, [65, 44], [65, 66]]]], ["content", "project.owner.name", ["loc", [null, [68, 44], [68, 66]]]], ["content", "project.owner.phone", ["loc", [null, [71, 44], [71, 67]]]], ["block", "unless", [["get", "user.isDeveloper", ["loc", [null, [75, 42], [75, 58]]]]], [], 0, null, ["loc", [null, [75, 32], [82, 43]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child6 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 119,
              "column": 40
            },
            "end": {
              "line": 119,
              "column": 104
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode(" Your team will be listed here ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child7 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 121,
                "column": 45
              },
              "end": {
                "line": 125,
                "column": 44
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 1,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                                            ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("a");
            dom.setAttribute(el1, "class", "dev-team__item");
            var el2 = dom.createTextNode("\n                                                ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("img");
            dom.setAttribute(el2, "alt", "");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                                            ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element16 = dom.childAt(fragment, [1, 1]);
            var morphs = new Array(1);
            morphs[0] = dom.createAttrMorph(element16, 'src');
            return morphs;
          },
          statements: [["attribute", "src", ["concat", [["get", "developer.image", ["loc", [null, [123, 60], [123, 75]]]]]]]],
          locals: ["developer"],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 119,
              "column": 104
            },
            "end": {
              "line": 127,
              "column": 40
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                                        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "dev-team__list");
          var el2 = dom.createTextNode("\n");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("                                        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 1, 1);
          return morphs;
        },
        statements: [["block", "each", [["get", "project.developers", ["loc", [null, [121, 53], [121, 71]]]]], [], 0, null, ["loc", [null, [121, 45], [125, 53]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child8 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 135,
                "column": 32
              },
              "end": {
                "line": 142,
                "column": 33
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                                    ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "pulsar");
            var el2 = dom.createTextNode("\n                                       ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "ring");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                                       ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "ring");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                                       ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "ring");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                                       ");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "ring");
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n                                    ");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 134,
              "column": 31
            },
            "end": {
              "line": 143,
              "column": 33
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "if", [["get", "project.isLinks", ["loc", [null, [135, 38], [135, 53]]]]], [], 0, null, ["loc", [null, [135, 32], [142, 40]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child9 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 145,
              "column": 32
            },
            "end": {
              "line": 148,
              "column": 32
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                                    ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          var el2 = dom.createElement("a");
          dom.setAttribute(el2, "class", " underline");
          dom.setAttribute(el2, "target", "_blank");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("i");
          dom.setAttribute(el2, "class", "glyphicon glyphicon-remove");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                                    ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element13 = dom.childAt(fragment, [1]);
          var element14 = dom.childAt(element13, [0]);
          var element15 = dom.childAt(element13, [1]);
          var morphs = new Array(3);
          morphs[0] = dom.createAttrMorph(element14, 'href');
          morphs[1] = dom.createMorphAt(element14, 0, 0);
          morphs[2] = dom.createElementMorph(element15);
          return morphs;
        },
        statements: [["attribute", "href", ["concat", [["get", "link.link", ["loc", [null, [146, 72], [146, 81]]]]]]], ["content", "link.title", ["loc", [null, [146, 101], [146, 115]]]], ["element", "action", ["deleteLink", ["get", "link", ["loc", [null, [146, 144], [146, 148]]]]], [], ["loc", [null, [146, 122], [146, 150]]]]],
        locals: ["link"],
        templates: []
      };
    })();
    var child10 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 171,
              "column": 21
            },
            "end": {
              "line": 173,
              "column": 21
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "btn btn-warning");
          dom.setAttribute(el1, "data-toggle", "modal");
          dom.setAttribute(el1, "data-target", "#report");
          var el2 = dom.createTextNode("Add Report");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child11 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 181,
              "column": 61
            },
            "end": {
              "line": 184,
              "column": 28
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-pencil text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element12 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element12);
          morphs[1] = dom.createMorphAt(dom.childAt(fragment, [3]), 0, 0);
          return morphs;
        },
        statements: [["element", "action", ["addDate"], [], ["loc", [null, [182, 34], [182, 54]]]], ["content", "project.deadline", ["loc", [null, [183, 33], [183, 53]]]]],
        locals: [],
        templates: []
      };
    })();
    var child12 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 184,
              "column": 28
            },
            "end": {
              "line": 190,
              "column": 28
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-floppy-o text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n\n                            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element11 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element11);
          morphs[1] = dom.createMorphAt(dom.childAt(fragment, [3]), 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["saveDate"], [], ["loc", [null, [185, 34], [185, 55]]]], ["inline", "input-mask", [], ["mask", "99/99/9999", "value", ["subexpr", "@mut", [["get", "project.deadline", ["loc", [null, [187, 70], [187, 86]]]]], [], []], "insert-newline", "saveDate", "class", "textareas form-control", "placeholder", "Enter daedlines"], ["loc", [null, [187, 32], [187, 175]]]]],
        locals: [],
        templates: []
      };
    })();
    var child13 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 193,
              "column": 46
            },
            "end": {
              "line": 194,
              "column": 155
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-pencil text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element10 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element10);
          morphs[1] = dom.createMorphAt(fragment, 3, 3, contextualElement);
          return morphs;
        },
        statements: [["element", "action", ["addBlock"], [], ["loc", [null, [194, 34], [194, 55]]]], ["inline", "convert-textarea", [], ["value", ["subexpr", "@mut", [["get", "project.roadblocks", ["loc", [null, [194, 134], [194, 152]]]]], [], []]], ["loc", [null, [194, 109], [194, 154]]]]],
        locals: [],
        templates: []
      };
    })();
    var child14 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 194,
              "column": 155
            },
            "end": {
              "line": 199,
              "column": 28
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-floppy-o text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element9 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element9);
          morphs[1] = dom.createMorphAt(dom.childAt(fragment, [3]), 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["saveRoadblocks"], [], ["loc", [null, [195, 34], [195, 61]]]], ["inline", "message-textarea", [], ["class", "textareas form-control", "value", ["subexpr", "@mut", [["get", "project.roadblocks", ["loc", [null, [197, 88], [197, 106]]]]], [], []], "action", "saveRoadblocks"], ["loc", [null, [197, 32], [197, 132]]]]],
        locals: [],
        templates: []
      };
    })();
    var child15 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 201,
              "column": 51
            },
            "end": {
              "line": 202,
              "column": 149
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-pencil text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element8 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element8);
          morphs[1] = dom.createMorphAt(fragment, 3, 3, contextualElement);
          return morphs;
        },
        statements: [["element", "action", ["addTask"], [], ["loc", [null, [202, 34], [202, 54]]]], ["inline", "convert-textarea", [], ["value", ["subexpr", "@mut", [["get", "project.tasks", ["loc", [null, [202, 133], [202, 146]]]]], [], []]], ["loc", [null, [202, 108], [202, 148]]]]],
        locals: [],
        templates: []
      };
    })();
    var child16 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 202,
              "column": 149
            },
            "end": {
              "line": 208,
              "column": 28
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          dom.setAttribute(el1, "class", "fa fa-icon fa-floppy-o text-light-gray");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                            ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "form-group");
          var el2 = dom.createTextNode("\n                                ");
          dom.appendChild(el1, el2);
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n\n                            ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element7 = dom.childAt(fragment, [1]);
          var morphs = new Array(2);
          morphs[0] = dom.createElementMorph(element7);
          morphs[1] = dom.createMorphAt(dom.childAt(fragment, [3]), 1, 1);
          return morphs;
        },
        statements: [["element", "action", ["saveTask"], [], ["loc", [null, [203, 34], [203, 55]]]], ["inline", "message-textarea", [], ["class", "textareas form-control", "value", ["subexpr", "@mut", [["get", "project.tasks", ["loc", [null, [205, 88], [205, 101]]]]], [], []], "action", "saveTask"], ["loc", [null, [205, 32], [205, 121]]]]],
        locals: [],
        templates: []
      };
    })();
    var child17 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 234,
                "column": 32
              },
              "end": {
                "line": 234,
                "column": 144
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode(" ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode(" ");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
            return morphs;
          },
          statements: [["inline", "message-textarea", [], ["class", "textareas form-control", "value", ["subexpr", "@mut", [["get", "report.body", ["loc", [null, [234, 110], [234, 121]]]]], [], []], "action", "saveReport"], ["loc", [null, [234, 54], [234, 143]]]]],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 234,
                "column": 144
              },
              "end": {
                "line": 234,
                "column": 192
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode(" ");
            dom.appendChild(el0, el1);
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode(" ");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var morphs = new Array(1);
            morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
            return morphs;
          },
          statements: [["inline", "convert-textarea", [], ["value", ["subexpr", "@mut", [["get", "report.body", ["loc", [null, [234, 178], [234, 189]]]]], [], []]], ["loc", [null, [234, 153], [234, 191]]]]],
          locals: [],
          templates: []
        };
      })();
      var child2 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 237,
                "column": 32
              },
              "end": {
                "line": 238,
                "column": 106
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("                                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("a");
            dom.setAttribute(el1, "class", "btn btn-xs btn-success");
            var el2 = dom.createTextNode("Save");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode(" ");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element1 = dom.childAt(fragment, [1]);
            var morphs = new Array(1);
            morphs[0] = dom.createElementMorph(element1);
            return morphs;
          },
          statements: [["element", "action", ["saveReport", ["get", "report", ["loc", [null, [238, 88], [238, 94]]]]], [], ["loc", [null, [238, 66], [238, 96]]]]],
          locals: [],
          templates: []
        };
      })();
      var child3 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 238,
                "column": 106
              },
              "end": {
                "line": 242,
                "column": 32
              }
            },
            "moduleName": "dts/templates/dashboard.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("\n                                \n                                ");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("a");
            dom.setAttribute(el1, "class", "editRepo btn btn-xs btn-info");
            var el2 = dom.createTextNode("Edit");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode(" \n\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1]);
            var morphs = new Array(1);
            morphs[0] = dom.createElementMorph(element0);
            return morphs;
          },
          statements: [["element", "action", ["editReport", ["get", "report", ["loc", [null, [240, 94], [240, 100]]]]], [], ["loc", [null, [240, 72], [240, 102]]]]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 226,
              "column": 24
            },
            "end": {
              "line": 247,
              "column": 24
            }
          },
          "moduleName": "dts/templates/dashboard.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                        ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "feed-item allreports");
          var el2 = dom.createTextNode("\n                            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("dt");
          dom.setAttribute(el2, "class", "feed-item__title");
          var el3 = dom.createTextNode("\n                              ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("img");
          dom.setAttribute(el3, "class", "feed-item__title__ava");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                              ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("h4");
          dom.setAttribute(el3, "class", "feed-item__title__name");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                              ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "feed-item__title__name");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                           ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("dd");
          dom.setAttribute(el2, "class", "feed-item__content");
          var el3 = dom.createTextNode("\n                                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("\n                            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                            ");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("dd");
          dom.setAttribute(el2, "class", "feed-item__control");
          var el3 = dom.createTextNode("\n");
          dom.appendChild(el2, el3);
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode("                                ");
          dom.appendChild(el2, el3);
          var el3 = dom.createElement("a");
          dom.setAttribute(el3, "class", "btn btn-xs btn-primary");
          var el4 = dom.createTextNode("Reply");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          var el3 = dom.createTextNode(" \n                            ");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n                        ");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element2 = dom.childAt(fragment, [1]);
          var element3 = dom.childAt(element2, [1]);
          var element4 = dom.childAt(element3, [1]);
          var element5 = dom.childAt(element2, [5]);
          var element6 = dom.childAt(element5, [3]);
          var morphs = new Array(9);
          morphs[0] = dom.createAttrMorph(element2, 'data-role');
          morphs[1] = dom.createAttrMorph(element2, 'data-id');
          morphs[2] = dom.createAttrMorph(element2, 'data-user');
          morphs[3] = dom.createAttrMorph(element4, 'src');
          morphs[4] = dom.createMorphAt(dom.childAt(element3, [3]), 0, 0);
          morphs[5] = dom.createMorphAt(dom.childAt(element3, [5]), 0, 0);
          morphs[6] = dom.createMorphAt(dom.childAt(element2, [3]), 1, 1);
          morphs[7] = dom.createMorphAt(element5, 1, 1);
          morphs[8] = dom.createAttrMorph(element6, 'href');
          return morphs;
        },
        statements: [["attribute", "data-role", ["get", "user.role", ["loc", [null, [227, 70], [227, 79]]]]], ["attribute", "data-id", ["get", "report.owner.id", ["loc", [null, [227, 92], [227, 107]]]]], ["attribute", "data-user", ["get", "user.id", ["loc", [null, [227, 122], [227, 129]]]]], ["attribute", "src", ["concat", [["get", "report.owner.image", ["loc", [null, [229, 72], [229, 90]]]]]]], ["content", "report.owner.name", ["loc", [null, [230, 65], [230, 86]]]], ["content", "report.formattedDate", ["loc", [null, [231, 67], [231, 91]]]], ["block", "if", [["get", "report.isEdit", ["loc", [null, [234, 38], [234, 51]]]]], [], 0, 1, ["loc", [null, [234, 32], [234, 199]]]], ["block", "if", [["get", "report.isEdit", ["loc", [null, [237, 38], [237, 51]]]]], [], 2, 3, ["loc", [null, [237, 32], [242, 39]]]], ["attribute", "href", ["concat", ["\n                                mailto:", ["get", "report.owner.mail", ["loc", [null, [244, 41], [244, 58]]]], "%20?subject=DTS,%20report%20reply,%20", ["get", "project.title", ["loc", [null, [244, 99], [244, 112]]]], ",%20", ["get", "user.name", ["loc", [null, [244, 120], [244, 129]]]], "&cc=", ["get", "project.manager.mail", ["loc", [null, [244, 137], [244, 157]]]], ",%20reports@devteam.space&body=Hi%20", ["get", "report.owner.name", ["loc", [null, [244, 197], [244, 214]]]], "!%0A%0A__Dear%20", ["get", "user.firstName", ["loc", [null, [244, 234], [244, 248]]]], ",%20please%20write%20your%20reply%20here__%0A%0A", ["get", "project.title", ["loc", [null, [244, 300], [244, 313]]]], ",%20report%20from%20​", ["get", "report.owner.name", ["loc", [null, [244, 338], [244, 355]]]], ",%20​", ["get", "report.formattedDate", ["loc", [null, [244, 364], [244, 384]]]], "%0A%0A", ["get", "report.body", ["loc", [null, [244, 394], [244, 405]]]], "%0A%0AThanks,%0A", ["get", "user.name", ["loc", [null, [244, 425], [244, 434]]]]]]]],
        locals: ["report"],
        templates: [child0, child1, child2, child3]
      };
    })();
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type", "multiple-nodes"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 281,
            "column": 6
          }
        },
        "moduleName": "dts/templates/dashboard.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "class", "header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-8");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-4 text-right");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode(" ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode(" | ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("a");
        dom.setAttribute(el4, "class", "underline");
        var el5 = dom.createTextNode("Logout");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-6");
        dom.setAttribute(el3, "id", "l-col-1");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "l-left");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "jsLeft");
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "customscroll");
        dom.setAttribute(el6, "style", "overflow: scroll;");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "project-form");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("form");
        dom.setAttribute(el8, "role", "form");
        var el9 = dom.createTextNode("\n");
        dom.appendChild(el8, el9);
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("                                \n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "pm-card");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "media");
        var el9 = dom.createTextNode("\n                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("div");
        dom.setAttribute(el9, "class", "media-body");
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        var el10 = dom.createElement("div");
        var el11 = dom.createElement("h4");
        dom.setAttribute(el11, "class", "media-heading");
        var el12 = dom.createTextNode("Project Manager");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        var el10 = dom.createElement("div");
        dom.setAttribute(el10, "class", "row");
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createElement("div");
        dom.setAttribute(el11, "class", "col-xs-4 col-sm-3 col-md-3");
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("img");
        dom.setAttribute(el12, "class", "media-object");
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                        ");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createElement("div");
        dom.setAttribute(el11, "class", "col-xs-4 col-sm-5 col-md-5");
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("p");
        dom.setAttribute(el12, "class", "pm-card__row");
        var el13 = dom.createComment("");
        dom.appendChild(el12, el13);
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("p");
        dom.setAttribute(el12, "class", "pm-card__row");
        var el13 = dom.createTextNode("Completed ");
        dom.appendChild(el12, el13);
        var el13 = dom.createComment("");
        dom.appendChild(el12, el13);
        var el13 = dom.createTextNode(" projects");
        dom.appendChild(el12, el13);
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("p");
        dom.setAttribute(el12, "class", "pm-card__row");
        var el13 = dom.createTextNode("Managed up to ");
        dom.appendChild(el12, el13);
        var el13 = dom.createComment("");
        dom.appendChild(el12, el13);
        var el13 = dom.createTextNode(" deployers");
        dom.appendChild(el12, el13);
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                        ");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createElement("div");
        dom.setAttribute(el11, "class", "col-xs-4 col-sm-4 col-md-4");
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("p");
        dom.setAttribute(el12, "class", "pm-card__row one-row");
        var el13 = dom.createElement("a");
        dom.setAttribute(el13, "class", "underline");
        var el14 = dom.createComment("");
        dom.appendChild(el13, el14);
        dom.appendChild(el12, el13);
        var el13 = dom.createTextNode("\n                                            ");
        dom.appendChild(el12, el13);
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                            ");
        dom.appendChild(el11, el12);
        var el12 = dom.createElement("p");
        dom.setAttribute(el12, "class", "pm-card__row one-row");
        var el13 = dom.createElement("a");
        dom.setAttribute(el13, "class", "underline");
        var el14 = dom.createComment("");
        dom.appendChild(el13, el14);
        dom.appendChild(el12, el13);
        var el13 = dom.createTextNode("\n                                            ");
        dom.appendChild(el12, el13);
        dom.appendChild(el11, el12);
        var el12 = dom.createTextNode("\n                                        ");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                    ");
        dom.appendChild(el10, el11);
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "dev-team");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "dev-team__table");
        var el9 = dom.createTextNode("\n                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("div");
        dom.setAttribute(el9, "class", "dev-team__row");
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        var el10 = dom.createElement("div");
        dom.setAttribute(el10, "class", "dev-team__cell dev-team__cell_min");
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createElement("div");
        dom.setAttribute(el11, "class", "dev-team__label");
        var el12 = dom.createTextNode("\n                                           Dev Team\n                                        ");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                    ");
        dom.appendChild(el10, el11);
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        var el10 = dom.createElement("div");
        dom.setAttribute(el10, "class", "dev-team__cell top1");
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createComment("");
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                    ");
        dom.appendChild(el10, el11);
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "custom-links ng-scope");
        dom.setAttribute(el7, "ng-controller", "docsController");
        var el8 = dom.createTextNode("\n");
        dom.appendChild(el7, el8);
        var el8 = dom.createComment("");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "custom-links__list docLinks");
        dom.setAttribute(el8, "style", "display: inline-block");
        var el9 = dom.createTextNode("\n");
        dom.appendChild(el8, el9);
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("button");
        dom.setAttribute(el9, "class", "btn btn-success");
        dom.setAttribute(el9, "style", "display: block");
        dom.setAttribute(el9, "data-toggle", "modal");
        dom.setAttribute(el9, "data-target", "#link");
        var el10 = dom.createTextNode("Add Link");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            \n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-6 hiddenBtn_true");
        dom.setAttribute(el3, "id", "l-col-2");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "jsRight ng-scope");
        dom.setAttribute(el4, "ng-controller", "statusbarController");
        dom.setAttribute(el4, "style", "width: 555px;");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "project-status");
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "progress-box ng-scope");
        dom.setAttribute(el6, "ng-controller", "statusBar");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("div class=\"percentage-cur\" ng-init=\"selectedRange=0\"");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "percentage-cur");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("span");
        dom.setAttribute(el8, "class", "num");
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("%");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "progress-bar progress-bar-slider");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "inner");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "project-status-btn text-center ");
        var el6 = dom.createTextNode("\n");
        dom.appendChild(el5, el6);
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("br");
        dom.setAttribute(el4, "clear", "all");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "feed-content");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("dl");
        dom.setAttribute(el7, "class", "feed-item");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("b");
        var el9 = dom.createTextNode("Estimated Completion Date");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode(" ");
        dom.appendChild(el7, el8);
        var el8 = dom.createComment("");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("br");
        dom.setAttribute(el8, "clear", "all");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("b");
        var el9 = dom.createTextNode("Roadblocks");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode(" ");
        dom.appendChild(el7, el8);
        var el8 = dom.createComment("");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("br");
        dom.setAttribute(el8, "clear", "all");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("b");
        var el9 = dom.createTextNode("Remaining Tasks");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode(" ");
        dom.appendChild(el7, el8);
        var el8 = dom.createComment("");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createComment(" <div class=\"form-group\">\n                           {{input value=tasks class=\"textareas form-control\"  insert-newline='addTask'  placeholder=\"Enter task\"}} <span {{action 'addTask'}} class=\"fa fa-icon fa-plus text-light-gray\"></span>\n                           <br clear=\"all\"/>\n                           <ul class=\"list-unstyled\">\n{{#each project.tasks as |block|}}\n                              <li>\n                                 <span class=\"{{block.isComplite}}\">{{block.name}}</span>\n                                  <span {{action 'compliteTask' block}} class=\"fa fa-icon fa-trash text-light-gray\"></span>\n                                   <span {{action 'deleteTask' block}} class=\"fa fa-icon fa-trash text-light-gray\"></span>\n                              </li>\n                           {{/each}}                           </ul>\n                        <br>\n                        </div>");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "feed");
        var el7 = dom.createTextNode("\n");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "modal fade");
        dom.setAttribute(el1, "id", "report");
        dom.setAttribute(el1, "tabindex", "-1");
        dom.setAttribute(el1, "role", "dialog");
        dom.setAttribute(el1, "aria-hidden", "true");
        var el2 = dom.createTextNode("\n  ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "modal-dialog");
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "modal-content");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-header");
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "close");
        dom.setAttribute(el5, "data-dismiss", "modal");
        dom.setAttribute(el5, "aria-hidden", "true");
        var el6 = dom.createTextNode("×");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("h4");
        dom.setAttribute(el5, "class", "modal-title ng-binding");
        var el6 = dom.createTextNode("Add Report");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-body");
        dom.setAttribute(el4, "ng-transclude", "");
        var el5 = dom.createTextNode("\n                  ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("form");
        var el6 = dom.createTextNode("\n                     ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "form-group");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                     ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                     ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "form-group text-center");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("a");
        dom.setAttribute(el7, "class", "custom-links__item  btn btn-warning btn-md");
        var el8 = dom.createTextNode("Save Report");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                     ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                  ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n   ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "modal fade");
        dom.setAttribute(el1, "id", "link");
        dom.setAttribute(el1, "tabindex", "-1");
        dom.setAttribute(el1, "role", "dialog");
        dom.setAttribute(el1, "aria-hidden", "true");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "modal-dialog");
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "modal-content");
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-header");
        var el5 = dom.createElement("button");
        dom.setAttribute(el5, "type", "button");
        dom.setAttribute(el5, "class", "close");
        dom.setAttribute(el5, "data-dismiss", "modal");
        dom.setAttribute(el5, "aria-hidden", "true");
        var el6 = dom.createTextNode("×");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("h4");
        dom.setAttribute(el5, "class", "modal-title ng-binding");
        var el6 = dom.createTextNode("New Link");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "class", "modal-body");
        dom.setAttribute(el4, "ng-transclude", "");
        var el5 = dom.createTextNode("\n                           ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("form");
        dom.setAttribute(el5, "class", "ng-scope ng-pristine ng-valid");
        var el6 = dom.createTextNode("\n                              ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "form-group");
        var el7 = dom.createTextNode("\n                                 ");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                              ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                              ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "form-group");
        var el7 = dom.createTextNode("\n                                 ");
        dom.appendChild(el6, el7);
        var el7 = dom.createComment("");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                              ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                              ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "form-group text-center");
        var el7 = dom.createTextNode("\n                                 ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("a");
        dom.setAttribute(el7, "class", "custom-links__item links btn btn-success");
        var el8 = dom.createTextNode("Add link");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                              ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                           ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n                        ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element25 = dom.childAt(fragment, [3, 1]);
        var element26 = dom.childAt(element25, [3]);
        var element27 = dom.childAt(element26, [5]);
        var element28 = dom.childAt(fragment, [5, 1]);
        var element29 = dom.childAt(element28, [1, 1, 1, 1]);
        var element30 = dom.childAt(element29, [1, 1]);
        var element31 = dom.childAt(element29, [3, 1, 1, 3]);
        var element32 = dom.childAt(element31, [1, 1]);
        var element33 = dom.childAt(element31, [3]);
        var element34 = dom.childAt(element31, [5]);
        var element35 = dom.childAt(element34, [1, 0]);
        var element36 = dom.childAt(element34, [3, 0]);
        var element37 = dom.childAt(element29, [7]);
        var element38 = dom.childAt(element28, [3]);
        var element39 = dom.childAt(element38, [1]);
        var element40 = dom.childAt(element39, [1]);
        var element41 = dom.childAt(element40, [1]);
        var element42 = dom.childAt(element41, [5, 1]);
        var element43 = dom.childAt(element38, [5, 1]);
        var element44 = dom.childAt(element43, [1, 1]);
        var element45 = dom.childAt(fragment, [7, 1, 0, 1, 1]);
        var element46 = dom.childAt(element45, [3, 1]);
        var element47 = dom.childAt(fragment, [9, 1, 0, 1, 1]);
        var element48 = dom.childAt(element47, [5, 1]);
        var morphs = new Array(31);
        morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
        morphs[1] = dom.createMorphAt(dom.childAt(element25, [1]), 1, 1);
        morphs[2] = dom.createMorphAt(element26, 1, 1);
        morphs[3] = dom.createMorphAt(element26, 3, 3);
        morphs[4] = dom.createElementMorph(element27);
        morphs[5] = dom.createMorphAt(element30, 1, 1);
        morphs[6] = dom.createMorphAt(element30, 2, 2);
        morphs[7] = dom.createAttrMorph(element32, 'src');
        morphs[8] = dom.createMorphAt(dom.childAt(element33, [1]), 0, 0);
        morphs[9] = dom.createMorphAt(dom.childAt(element33, [3]), 1, 1);
        morphs[10] = dom.createMorphAt(dom.childAt(element33, [5]), 1, 1);
        morphs[11] = dom.createAttrMorph(element35, 'href');
        morphs[12] = dom.createMorphAt(element35, 0, 0);
        morphs[13] = dom.createAttrMorph(element36, 'href');
        morphs[14] = dom.createMorphAt(element36, 0, 0);
        morphs[15] = dom.createMorphAt(dom.childAt(element29, [5, 1, 1, 3]), 1, 1);
        morphs[16] = dom.createMorphAt(element37, 1, 1);
        morphs[17] = dom.createMorphAt(dom.childAt(element37, [3]), 1, 1);
        morphs[18] = dom.createElementMorph(element40);
        morphs[19] = dom.createMorphAt(dom.childAt(element41, [3, 1]), 0, 0);
        morphs[20] = dom.createAttrMorph(element42, 'style');
        morphs[21] = dom.createMorphAt(dom.childAt(element39, [3]), 1, 1);
        morphs[22] = dom.createMorphAt(element44, 3, 3);
        morphs[23] = dom.createMorphAt(element44, 9, 9);
        morphs[24] = dom.createMorphAt(element44, 15, 15);
        morphs[25] = dom.createMorphAt(dom.childAt(element43, [3]), 1, 1);
        morphs[26] = dom.createMorphAt(dom.childAt(element45, [1]), 1, 1);
        morphs[27] = dom.createElementMorph(element46);
        morphs[28] = dom.createMorphAt(dom.childAt(element47, [1]), 1, 1);
        morphs[29] = dom.createMorphAt(dom.childAt(element47, [3]), 1, 1);
        morphs[30] = dom.createElementMorph(element48);
        return morphs;
      },
      statements: [["content", "init-component", ["loc", [null, [2, 0], [2, 18]]]], ["block", "link-to", ["projects"], ["class", "header__logo"], 0, null, ["loc", [null, [6, 12], [6, 81]]]], ["block", "if", [["get", "user.isSuperAdmin", ["loc", [null, [9, 18], [9, 35]]]]], [], 1, null, ["loc", [null, [9, 12], [9, 117]]]], ["block", "link-to", ["projects"], ["class", "underline"], 2, null, ["loc", [null, [9, 118], [9, 179]]]], ["element", "action", ["logOut"], [], ["loc", [null, [9, 185], [9, 204]]]], ["block", "if", [["get", "project.isNew", ["loc", [null, [21, 39], [21, 52]]]]], [], 3, null, ["loc", [null, [21, 33], [28, 40]]]], ["block", "if", [["get", "project.isEdit", ["loc", [null, [29, 38], [29, 52]]]]], [], 4, 5, ["loc", [null, [29, 32], [83, 39]]]], ["attribute", "src", ["concat", [["get", "project.manager.image", ["loc", [null, [93, 77], [93, 98]]]]]]], ["content", "project.manager.name", ["loc", [null, [96, 68], [96, 92]]]], ["content", "project.manager.finished", ["loc", [null, [97, 78], [97, 106]]]], ["content", "project.manager.managed", ["loc", [null, [98, 82], [98, 109]]]], ["attribute", "href", ["concat", ["tel:", ["get", "project.manager.phone", ["loc", [null, [101, 91], [101, 112]]]]]]], ["content", "project.manager.phone", ["loc", [null, [101, 134], [101, 159]]]], ["attribute", "href", ["concat", ["mailto:", ["get", "project.manager.mail", ["loc", [null, [103, 94], [103, 114]]]]]]], ["content", "project.manager.mail", ["loc", [null, [103, 136], [103, 160]]]], ["block", "if", [["get", "project.isEmptyDevelopers", ["loc", [null, [119, 46], [119, 71]]]]], [], 6, 7, ["loc", [null, [119, 40], [127, 47]]]], ["block", "if", [["get", "user.isCustomer", ["loc", [null, [134, 37], [134, 52]]]]], [], 8, null, ["loc", [null, [134, 31], [143, 40]]]], ["block", "each", [["get", "project.links", ["loc", [null, [145, 40], [145, 53]]]]], [], 9, null, ["loc", [null, [145, 32], [148, 41]]]], ["element", "action", ["setStatus"], [], ["loc", [null, [159, 44], [159, 66]]]], ["content", "project.status", ["loc", [null, [163, 46], [163, 64]]]], ["attribute", "style", ["concat", ["width: ", ["get", "project.status", ["loc", [null, [166, 63], [166, 77]]]], "%;"]]], ["block", "unless", [["get", "user.isCustomer", ["loc", [null, [171, 31], [171, 46]]]]], [], 10, null, ["loc", [null, [171, 21], [173, 32]]]], ["block", "unless", [["get", "isEstimated", ["loc", [null, [181, 71], [181, 82]]]]], [], 11, 12, ["loc", [null, [181, 61], [190, 39]]]], ["block", "unless", [["get", "isRoad", ["loc", [null, [193, 56], [193, 62]]]]], [], 13, 14, ["loc", [null, [193, 46], [199, 39]]]], ["block", "unless", [["get", "isTask", ["loc", [null, [201, 61], [201, 67]]]]], [], 15, 16, ["loc", [null, [201, 51], [208, 39]]]], ["block", "each", [["get", "project.sortedItems", ["loc", [null, [226, 32], [226, 51]]]]], [], 17, null, ["loc", [null, [226, 24], [247, 33]]]], ["inline", "textarea", [], ["rows", "6", "class", "form-control", "value", ["subexpr", "@mut", [["get", "report", ["loc", [null, [259, 71], [259, 77]]]]], [], []]], ["loc", [null, [259, 24], [259, 79]]]], ["element", "action", ["newRepotr"], [], ["loc", [null, [262, 78], [262, 100]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "linkTitle", ["loc", [null, [271, 47], [271, 56]]]]], [], []], "placeholder", "Title", "class", "form-control"], ["loc", [null, [271, 33], [271, 99]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "linkHref", ["loc", [null, [274, 47], [274, 55]]]]], [], []], "placeholder", "Link", "class", "form-control"], ["loc", [null, [274, 33], [274, 97]]]], ["element", "action", ["addLink"], [], ["loc", [null, [277, 85], [277, 105]]]]],
      locals: [],
      templates: [child0, child1, child2, child3, child4, child5, child6, child7, child8, child9, child10, child11, child12, child13, child14, child15, child16, child17]
    };
  })());
});
define("dts/templates/index", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["empty-body"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 4,
            "column": 0
          }
        },
        "moduleName": "dts/templates/index.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("\n\n\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/login", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 35,
              "column": 28
            },
            "end": {
              "line": 35,
              "column": 55
            }
          },
          "moduleName": "dts/templates/login.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Signup");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "fragmentReason": {
          "name": "triple-curlies"
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 54,
            "column": 6
          }
        },
        "moduleName": "dts/templates/login.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "page-content ng-scope");
        var el2 = dom.createTextNode("\n\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "space-6");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-sm-6 col-sm-offset-3");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "id", "login-box");
        dom.setAttribute(el4, "class", "login-box visible widget-box no-border");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "widget-body");
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "widget-main");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("h4");
        dom.setAttribute(el7, "class", "lighter bigger text-center");
        var el8 = dom.createTextNode("Login to DevTeam.Space");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("div");
        dom.setAttribute(el7, "class", "space-16");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("form");
        dom.setAttribute(el7, "name", "loginForm");
        dom.setAttribute(el7, "class", "form-horizontal ng-pristine ng-invalid ng-invalid-required");
        dom.setAttribute(el7, "role", "form");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n	                                	");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n		                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n                                    	");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n		                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "space");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "clearfix");
        var el9 = dom.createTextNode("\n                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("div");
        dom.setAttribute(el9, "class", "row");
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        var el10 = dom.createElement("div");
        dom.setAttribute(el10, "class", "col-sm-12 text-center");
        var el11 = dom.createTextNode("\n                                        ");
        dom.appendChild(el10, el11);
        var el11 = dom.createElement("button");
        dom.setAttribute(el11, "type", "submit");
        dom.setAttribute(el11, "class", "btn btn-lg btn-primary");
        var el12 = dom.createTextNode("Login");
        dom.appendChild(el11, el12);
        dom.appendChild(el10, el11);
        var el11 = dom.createTextNode("\n                                    ");
        dom.appendChild(el10, el11);
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "space-16");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "space-4");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "col-sm-12 text-center");
        var el9 = dom.createTextNode("Don't have an account? \n                            ");
        dom.appendChild(el8, el9);
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n\n\n\n                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createComment(" /widget-main ");
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n\n\n                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment(" /widget-body ");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment(" /login-box ");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment(" /position-relative ");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1, 3, 1, 1, 1, 5]);
        var element1 = dom.childAt(element0, [7, 1, 1, 1]);
        var morphs = new Array(4);
        morphs[0] = dom.createMorphAt(dom.childAt(element0, [1, 1]), 1, 1);
        morphs[1] = dom.createMorphAt(dom.childAt(element0, [3, 1]), 1, 1);
        morphs[2] = dom.createElementMorph(element1);
        morphs[3] = dom.createMorphAt(dom.childAt(element0, [13]), 1, 1);
        return morphs;
      },
      statements: [["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "email", ["loc", [null, [14, 48], [14, 53]]]]], [], []], "placeholder", "Email", "class", "form-control"], ["loc", [null, [14, 34], [14, 96]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "password", ["loc", [null, [19, 51], [19, 59]]]]], [], []], "type", "password", "placeholder", "Password", "class", "form-control"], ["loc", [null, [19, 37], [19, 121]]]], ["element", "action", ["login"], [], ["loc", [null, [28, 93], [28, 111]]]], ["block", "link-to", ["signup"], [], 0, null, ["loc", [null, [35, 28], [35, 67]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("dts/templates/modals/status", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["empty-body"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 0
          }
        },
        "moduleName": "dts/templates/modals/status.hbs"
      },
      isEmpty: true,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("dts/templates/projects", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 6,
              "column": 12
            },
            "end": {
              "line": 6,
              "column": 69
            }
          },
          "moduleName": "dts/templates/projects.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("DevTeam.Space");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 9,
                "column": 38
              },
              "end": {
                "line": 9,
                "column": 95
              }
            },
            "moduleName": "dts/templates/projects.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Administrate");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 9,
              "column": 12
            },
            "end": {
              "line": 9,
              "column": 110
            }
          },
          "moduleName": "dts/templates/projects.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode(" | ");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["block", "link-to", ["administrate"], ["class", "underline"], 0, null, ["loc", [null, [9, 38], [9, 107]]]]],
        locals: [],
        templates: [child0]
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 9,
              "column": 118
            },
            "end": {
              "line": 9,
              "column": 167
            }
          },
          "moduleName": "dts/templates/projects.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Projects");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child3 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 15,
              "column": 8
            },
            "end": {
              "line": 15,
              "column": 124
            }
          },
          "moduleName": "dts/templates/projects.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode(" ");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("button");
          dom.setAttribute(el1, "class", "btn btn-success circle btn-lg btn-block");
          var el2 = dom.createTextNode("New");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element2 = dom.childAt(fragment, [1]);
          var morphs = new Array(1);
          morphs[0] = dom.createElementMorph(element2);
          return morphs;
        },
        statements: [["element", "action", ["newProject"], [], ["loc", [null, [15, 40], [15, 63]]]]],
        locals: [],
        templates: []
      };
    })();
    var child4 = (function () {
      var child0 = (function () {
        var child0 = (function () {
          return {
            meta: {
              "fragmentReason": false,
              "revision": "Ember@2.2.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 22,
                  "column": 2
                },
                "end": {
                  "line": 25,
                  "column": 2
                }
              },
              "moduleName": "dts/templates/projects.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("				");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("h4");
              dom.setAttribute(el1, "class", "");
              var el2 = dom.createElement("strong");
              var el3 = dom.createComment("");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n				");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("p");
              dom.setAttribute(el1, "class", "hideString");
              var el2 = dom.createComment("");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
              var morphs = new Array(2);
              morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1, 0]), 0, 0);
              morphs[1] = dom.createMorphAt(dom.childAt(fragment, [3]), 0, 0);
              return morphs;
            },
            statements: [["content", "project.title", ["loc", [null, [23, 26], [23, 43]]]], ["content", "project.description", ["loc", [null, [24, 26], [24, 49]]]]],
            locals: [],
            templates: []
          };
        })();
        var child1 = (function () {
          return {
            meta: {
              "fragmentReason": false,
              "revision": "Ember@2.2.0",
              "loc": {
                "source": null,
                "start": {
                  "line": 25,
                  "column": 2
                },
                "end": {
                  "line": 30,
                  "column": 2
                }
              },
              "moduleName": "dts/templates/projects.hbs"
            },
            isEmpty: false,
            arity: 0,
            cachedFragment: null,
            hasRendered: false,
            buildFragment: function buildFragment(dom) {
              var el0 = dom.createDocumentFragment();
              var el1 = dom.createTextNode("				");
              dom.appendChild(el0, el1);
              var el1 = dom.createElement("div");
              var el2 = dom.createTextNode("\n					");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("h4");
              var el3 = dom.createElement("strong");
              var el4 = dom.createTextNode("New project");
              dom.appendChild(el3, el4);
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n					");
              dom.appendChild(el1, el2);
              var el2 = dom.createElement("p");
              var el3 = dom.createTextNode("Click on this card and share a bit more information about your project");
              dom.appendChild(el2, el3);
              dom.appendChild(el1, el2);
              var el2 = dom.createTextNode("\n				");
              dom.appendChild(el1, el2);
              dom.appendChild(el0, el1);
              var el1 = dom.createTextNode("\n");
              dom.appendChild(el0, el1);
              return el0;
            },
            buildRenderNodes: function buildRenderNodes() {
              return [];
            },
            statements: [],
            locals: [],
            templates: []
          };
        })();
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 21,
                "column": 4
              },
              "end": {
                "line": 44,
                "column": 1
              }
            },
            "moduleName": "dts/templates/projects.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createComment("");
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("				\n		");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "row project-form-info");
            var el2 = dom.createTextNode("\n			");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "col-xs-6 text-right border-r");
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createTextNode("Status");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createTextNode("Completion Date");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createTextNode("Blockers");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n			");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n			");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("div");
            dom.setAttribute(el2, "class", "col-xs-6 text-left");
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createComment("");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n				");
            dom.appendChild(el2, el3);
            var el3 = dom.createElement("p");
            var el4 = dom.createElement("span");
            dom.appendChild(el3, el4);
            dom.appendChild(el2, el3);
            var el3 = dom.createTextNode("\n			");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n		");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("		\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [2, 3]);
            var element1 = dom.childAt(element0, [5, 0]);
            var morphs = new Array(4);
            morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
            morphs[1] = dom.createMorphAt(dom.childAt(element0, [1]), 0, 0);
            morphs[2] = dom.createMorphAt(dom.childAt(element0, [3]), 0, 0);
            morphs[3] = dom.createAttrMorph(element1, 'class');
            dom.insertBoundary(fragment, 0);
            return morphs;
          },
          statements: [["block", "unless", [["get", "project.isNew", ["loc", [null, [22, 12], [22, 25]]]]], [], 0, 1, ["loc", [null, [22, 2], [30, 13]]]], ["content", "project.statusField", ["loc", [null, [39, 7], [39, 30]]]], ["content", "project.deadline", ["loc", [null, [40, 7], [40, 27]]]], ["attribute", "class", ["concat", ["progectStatus ", ["get", "project.blockers", ["loc", [null, [41, 36], [41, 52]]]]]]]],
          locals: [],
          templates: [child0, child1]
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 20,
              "column": 1
            },
            "end": {
              "line": 45,
              "column": 1
            }
          },
          "moduleName": "dts/templates/projects.hbs"
        },
        isEmpty: false,
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "link-to", ["dashboard", ["get", "project", ["loc", [null, [21, 27], [21, 34]]]]], ["class", "hoverBlock col-md-6 col-md-offset-3 feed-bg text-center"], 0, null, ["loc", [null, [21, 4], [44, 13]]]]],
        locals: ["project"],
        templates: [child0]
      };
    })();
    return {
      meta: {
        "fragmentReason": {
          "name": "missing-wrapper",
          "problems": ["wrong-type", "multiple-nodes"]
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 46,
            "column": 6
          }
        },
        "moduleName": "dts/templates/projects.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "class", "header");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-8");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-xs-12 col-sm-4 text-right");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode(" ");
        dom.appendChild(el3, el4);
        var el4 = dom.createComment("");
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode(" | ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("a");
        dom.setAttribute(el4, "class", "underline");
        var el5 = dom.createTextNode("Logout");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n	");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "row");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "col-md-6 col-md-offset-3 text-center");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("br");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "row");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element3 = dom.childAt(fragment, [3, 1]);
        var element4 = dom.childAt(element3, [3]);
        var element5 = dom.childAt(element4, [5]);
        var morphs = new Array(7);
        morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
        morphs[1] = dom.createMorphAt(dom.childAt(element3, [1]), 1, 1);
        morphs[2] = dom.createMorphAt(element4, 1, 1);
        morphs[3] = dom.createMorphAt(element4, 3, 3);
        morphs[4] = dom.createElementMorph(element5);
        morphs[5] = dom.createMorphAt(dom.childAt(fragment, [5, 1]), 1, 1);
        morphs[6] = dom.createMorphAt(dom.childAt(fragment, [9]), 1, 1);
        return morphs;
      },
      statements: [["content", "init-component", ["loc", [null, [2, 0], [2, 18]]]], ["block", "link-to", ["projects"], ["class", "header__logo"], 0, null, ["loc", [null, [6, 12], [6, 81]]]], ["block", "if", [["get", "user.isSuperAdmin", ["loc", [null, [9, 18], [9, 35]]]]], [], 1, null, ["loc", [null, [9, 12], [9, 117]]]], ["block", "link-to", ["projects"], ["class", "underline"], 2, null, ["loc", [null, [9, 118], [9, 179]]]], ["element", "action", ["logOut"], [], ["loc", [null, [9, 185], [9, 204]]]], ["block", "if", [["get", "user.isCustomer", ["loc", [null, [15, 14], [15, 29]]]]], [], 3, null, ["loc", [null, [15, 8], [15, 131]]]], ["block", "each", [["get", "user.sortedProjects", ["loc", [null, [20, 9], [20, 28]]]]], [], 4, null, ["loc", [null, [20, 1], [45, 10]]]]],
      locals: [],
      templates: [child0, child1, child2, child3, child4]
    };
  })());
});
define("dts/templates/signup", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 36,
                "column": 38
              },
              "end": {
                "line": 36,
                "column": 69
              }
            },
            "moduleName": "dts/templates/signup.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Your role");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      var child1 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 37,
                "column": 38
              },
              "end": {
                "line": 37,
                "column": 69
              }
            },
            "moduleName": "dts/templates/signup.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Customer");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      var child2 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 38,
                "column": 38
              },
              "end": {
                "line": 38,
                "column": 68
              }
            },
            "moduleName": "dts/templates/signup.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Manager");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      var child3 = (function () {
        return {
          meta: {
            "fragmentReason": false,
            "revision": "Ember@2.2.0",
            "loc": {
              "source": null,
              "start": {
                "line": 39,
                "column": 38
              },
              "end": {
                "line": 39,
                "column": 70
              }
            },
            "moduleName": "dts/templates/signup.hbs"
          },
          isEmpty: false,
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("Developer");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes() {
            return [];
          },
          statements: [],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 35,
              "column": 36
            },
            "end": {
              "line": 40,
              "column": 36
            }
          },
          "moduleName": "dts/templates/signup.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("                                      ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                      ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                      ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n                                      ");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(4);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          morphs[1] = dom.createMorphAt(fragment, 3, 3, contextualElement);
          morphs[2] = dom.createMorphAt(fragment, 5, 5, contextualElement);
          morphs[3] = dom.createMorphAt(fragment, 7, 7, contextualElement);
          return morphs;
        },
        statements: [["block", "x-option", [], ["value", ""], 0, null, ["loc", [null, [36, 38], [36, 82]]]], ["block", "x-option", [], ["value", "3"], 1, null, ["loc", [null, [37, 38], [37, 82]]]], ["block", "x-option", [], ["value", "1"], 2, null, ["loc", [null, [38, 38], [38, 81]]]], ["block", "x-option", [], ["value", "2"], 3, null, ["loc", [null, [39, 38], [39, 83]]]]],
        locals: [],
        templates: [child0, child1, child2, child3]
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "fragmentReason": false,
          "revision": "Ember@2.2.0",
          "loc": {
            "source": null,
            "start": {
              "line": 56,
              "column": 28
            },
            "end": {
              "line": 56,
              "column": 55
            }
          },
          "moduleName": "dts/templates/signup.hbs"
        },
        isEmpty: false,
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Sign In");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "fragmentReason": {
          "name": "triple-curlies"
        },
        "revision": "Ember@2.2.0",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 65,
            "column": 6
          }
        },
        "moduleName": "dts/templates/signup.hbs"
      },
      isEmpty: false,
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "page-content ng-scope");
        var el2 = dom.createTextNode("\n    ");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "row");
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "space-6");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n        ");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("div");
        dom.setAttribute(el3, "class", "col-sm-6 col-sm-offset-3");
        var el4 = dom.createTextNode("\n            ");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("div");
        dom.setAttribute(el4, "id", "login-box");
        dom.setAttribute(el4, "class", "login-box visible widget-box no-border");
        var el5 = dom.createTextNode("\n                ");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("div");
        dom.setAttribute(el5, "class", "widget-body");
        var el6 = dom.createTextNode("\n                    ");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("div");
        dom.setAttribute(el6, "class", "widget-main");
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("h4");
        dom.setAttribute(el7, "class", "text-center blue lighter bigger");
        var el8 = dom.createTextNode("\n                            Signup for DevTeam.Space\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("br");
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                        ");
        dom.appendChild(el6, el7);
        var el7 = dom.createElement("form");
        dom.setAttribute(el7, "name", "signupForm");
        dom.setAttribute(el7, "class", "form-horizontal ng-pristine ng-invalid ng-invalid-required ng-invalid-password-no-match");
        dom.setAttribute(el7, "role", "form");
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n                                        ");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n                                    ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n	                                ");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n	                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n                                	");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n	                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n                                	");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n	                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("	                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group");
        var el9 = dom.createTextNode("\n                                    ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("span");
        dom.setAttribute(el9, "class", "block input-icon input-icon-right");
        var el10 = dom.createTextNode("\n                                	");
        dom.appendChild(el9, el10);
        var el10 = dom.createComment("");
        dom.appendChild(el9, el10);
        var el10 = dom.createTextNode("\n	                            ");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "form-group text-center");
        var el9 = dom.createTextNode("\n                                ");
        dom.appendChild(el8, el9);
        var el9 = dom.createElement("button");
        dom.setAttribute(el9, "type", "submit");
        dom.setAttribute(el9, "class", "btn btn-lg btn-primary");
        var el10 = dom.createTextNode("Sign Up");
        dom.appendChild(el9, el10);
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "space-16");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "space-4");
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                            ");
        dom.appendChild(el7, el8);
        var el8 = dom.createElement("div");
        dom.setAttribute(el8, "class", "text-center");
        var el9 = dom.createTextNode("Already have an account? \n                            ");
        dom.appendChild(el8, el9);
        var el9 = dom.createComment("");
        dom.appendChild(el8, el9);
        var el9 = dom.createTextNode("\n                            ");
        dom.appendChild(el8, el9);
        dom.appendChild(el7, el8);
        var el8 = dom.createTextNode("\n                        ");
        dom.appendChild(el7, el8);
        dom.appendChild(el6, el7);
        var el7 = dom.createTextNode("\n                    ");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n                ");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n            ");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n\n        ");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n    ");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1, 3, 1, 1, 1, 5]);
        var element1 = dom.childAt(element0, [13, 1]);
        var morphs = new Array(8);
        morphs[0] = dom.createMorphAt(dom.childAt(element0, [1, 1]), 1, 1);
        morphs[1] = dom.createMorphAt(dom.childAt(element0, [3, 1]), 1, 1);
        morphs[2] = dom.createMorphAt(dom.childAt(element0, [5, 1]), 1, 1);
        morphs[3] = dom.createMorphAt(dom.childAt(element0, [7, 1]), 1, 1);
        morphs[4] = dom.createMorphAt(dom.childAt(element0, [9, 1]), 1, 1);
        morphs[5] = dom.createMorphAt(dom.childAt(element0, [11, 1]), 1, 1);
        morphs[6] = dom.createElementMorph(element1);
        morphs[7] = dom.createMorphAt(dom.childAt(element0, [19]), 1, 1);
        return morphs;
      },
      statements: [["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "email", ["loc", [null, [15, 54], [15, 59]]]]], [], []], "placeholder", "Email", "class", "form-control"], ["loc", [null, [15, 40], [15, 102]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "password", ["loc", [null, [20, 47], [20, 55]]]]], [], []], "type", "password", "placeholder", "Password", "class", "form-control"], ["loc", [null, [20, 33], [20, 117]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "name", ["loc", [null, [25, 47], [25, 51]]]]], [], []], "placeholder", "Name", "class", "form-control"], ["loc", [null, [25, 33], [25, 93]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "company", ["loc", [null, [30, 47], [30, 54]]]]], [], []], "placeholder", "Company", "class", "form-control"], ["loc", [null, [30, 33], [30, 99]]]], ["block", "x-select", [], ["value", ["subexpr", "@mut", [["get", "role", ["loc", [null, [35, 54], [35, 58]]]]], [], []], "class", "form-control"], 0, null, ["loc", [null, [35, 36], [40, 49]]]], ["inline", "input", [], ["value", ["subexpr", "@mut", [["get", "phone", ["loc", [null, [45, 47], [45, 52]]]]], [], []], "placeholder", "Phone", "class", "form-control"], ["loc", [null, [45, 33], [45, 95]]]], ["element", "action", ["signUp"], [], ["loc", [null, [50, 54], [50, 73]]]], ["block", "link-to", ["login"], [], 1, null, ["loc", [null, [56, 28], [56, 67]]]]],
      locals: [],
      templates: [child0, child1]
    };
  })());
});
define('dts/torii-providers/firebase', ['exports', 'emberfire/torii-providers/firebase'], function (exports, _emberfireToriiProvidersFirebase) {
  exports['default'] = _emberfireToriiProvidersFirebase['default'];
});
/* jshint ignore:start */

/* jshint ignore:end */

/* jshint ignore:start */

define('dts/config/environment', ['ember'], function(Ember) {
  var prefix = 'dts';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (!runningTests) {
  require("dts/app")["default"].create({"name":"dts","version":"0.0.0+75aa3842"});
}

/* jshint ignore:end */
//# sourceMappingURL=dts.map